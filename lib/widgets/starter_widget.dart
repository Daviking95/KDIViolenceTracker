import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/bottom_navigation.dart';
import 'package:template_app/widgets/temp_app_widget/fabs.dart';
import 'package:template_app/widgets/temp_app_widget/nav_drawer.dart';

class StarterWidget extends StatefulWidget {
  final Widget topPageContent;
  final Widget bottomPageContent;
  final bool isBgAllowed;
  final bool isFabAllowed;
  final bool hasTopWidget;
  final bool hasNavDrawer;
  final IconData fabIcon;
  final Function fabFunc;
  final bool centerBottomWidget;
  final bool hideBackgroundArts;
  final bool isBottomBarVisible;
  final int bottomNavIndex;

  StarterWidget(
      {this.topPageContent,
      this.bottomPageContent,
      this.isBgAllowed = false,
      this.isFabAllowed = false,
      this.hasTopWidget = false,
      this.hasNavDrawer = false,
      this.fabIcon,
      this.fabFunc,
      this.centerBottomWidget = false,
      this.hideBackgroundArts = false, this.isBottomBarVisible = false, this.bottomNavIndex = 0});

  @override
  _StarterWidgetState createState() => _StarterWidgetState();
}

class _StarterWidgetState extends State<StarterWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          body: Container(
            decoration: new BoxDecoration(),
            child: Stack(
              children: <Widget>[
                pageContent(),
                widget.hideBackgroundArts
                    ? Container()
                    : Positioned(
                        bottom: -100.0,
                        left: -150.0,
                        child: Container(
                          height: 300.0,
                          child: Image.asset(
                            ImageStringConstants.imgAppEllipsePrimary,
                          ),
                        ),
                      ),
                widget.hideBackgroundArts
                    ? Container()
                    : Positioned(
                        bottom: -30.0,
                        left: 250.0,
                        child: Container(
                          height: 100.0,
                          child: Image.asset(
                            ImageStringConstants.imgAppEllipseSecondary,
                          ),
                        ),
                      ),
                widget.hideBackgroundArts
                    ? Container()
                    : Positioned(
                        top: -80.0,
                        left: 150.0,
                        child: Container(
                          height: 300.0,
                          child: Image.asset(
                            ImageStringConstants.imgAppEllipsePrimaryFaded,
                          ),
                        ),
                      ),
                widget.hideBackgroundArts
                    ? Container()
                    : Positioned(
                        top: -100.0,
                        left: -200.0,
                        child: Container(
                          height: 300.0,
                          child: Image.asset(
                            ImageStringConstants.imgAppEllipseSecondaryFaded,
                          ),
                        ),
                      )
              ],
            ),
          ),
          drawer: widget.hasNavDrawer ? customNavDrawer(context) : Container(),
          backgroundColor:
              widget.isBgAllowed ? appPrimaryDarkColor : appWhiteColor,
          floatingActionButton: widget.isFabAllowed
              ? startFabWidget(
                  widget.fabFunc, appPrimaryDarkColor, widget.fabIcon)
              : Container(),
          bottomNavigationBar: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            height: widget.isBottomBarVisible ? 60.0 : 0.0,
            child: widget.isBottomBarVisible ? GBBottomNavigation(selectedIndex: widget.bottomNavIndex,) : Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
            ),
          ),
        ),
      ),
    );
  }

  Widget pageContent() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
//        padding: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          gradient: new LinearGradient(
              colors: [
                widget.isBgAllowed ? appPrimaryDarkColor : appBackgroundColor,
                widget.isBgAllowed ? appPrimaryDarkColor : appWhiteTwoColor
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
        ),
        child: Column(
          children: <Widget>[
            Row(
//            crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                widget.hasNavDrawer
                    ? IconButton(
                        icon: Icon(
                          Icons.menu,
                          color: appPrimaryDarkColor,
                        ),
                        onPressed: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                      )
                    : Container(),
                widget.hasNavDrawer ? Spacer() : Container(),
                widget.hasTopWidget ? widget.topPageContent : Container(),
              ],
            ),
            Expanded(
              child: widget.centerBottomWidget
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        widget.bottomPageContent,
                      ],
                    )
                  : Column(
//              mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        widget.bottomPageContent,
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
