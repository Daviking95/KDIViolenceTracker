
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

alertDialogWithText({BuildContext context, String title, String desc}){
  Alert(context: context, style: alertStyle, title: title, desc: desc).show();
}

alertDialogWithOneButton({BuildContext context, String title, String desc, String btnText, Function function, AlertType alertType}){
  Alert(
    context: context,
    style: alertStyle,
    type: alertType, //AlertType.error,
    title: title,
    desc: desc,
    buttons: [
      DialogButton(
        child: Text(
          btnText,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => function(),
        width: 120,
      )
    ],
  ).show();
}

alertDialogWithTwoButton({BuildContext context, String title, String desc = "", String btnText1, String btnText2, Function function1, Function function2, AlertType alertType}){
  Alert(
    context: context,
    style: alertStyle,
    type: alertType, //AlertType.warning,
    title: title,
    desc: desc,
    buttons: [
      DialogButton(
        child: Text(
          btnText1,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => function1(),
        color: Color.fromRGBO(0, 179, 134, 1.0),
      ),
      DialogButton(
        child: Text(
          btnText2,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => function2(),
        gradient: LinearGradient(colors: [
          Color.fromRGBO(116, 116, 191, 1.0),
          Color.fromRGBO(52, 138, 199, 1.0)
        ]),
      )
    ],
  ).show();
}

alertDialogWithImage({BuildContext context, String title, String desc, String imgUrl}){
  Alert(
    context: context,
    title: title,
    desc: desc,
    image: Image.asset(imgUrl),
  ).show();
}

var alertStyle = AlertStyle(
  animationType: AnimationType.fromTop,
  isCloseButton: false,
  isOverlayTapDismiss: false,
  descStyle: TextStyle(fontWeight: FontWeight.bold),
  animationDuration: Duration(milliseconds: 400),
  alertBorder: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(0.0),
    side: BorderSide(
      color: Colors.grey,
    ),
  ),
  titleStyle: TextStyle(
    color: Colors.red,
  ),
);


// link to this resources : https://pub.dev/packages/rflutter_alert