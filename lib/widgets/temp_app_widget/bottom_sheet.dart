

import 'package:flutter/material.dart';

modalBottomSheet(BuildContext context, Widget widget) {
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (builder) {
        return Container(
          color: Color(0xFF737373),
          child: new Container(
            padding: EdgeInsets.all(15.0),
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0))),
            child: widget,
          ),
        );
      });
}






// link to resources : https://pub.dev/packages/solid_bottom_sheet#-example-tab-