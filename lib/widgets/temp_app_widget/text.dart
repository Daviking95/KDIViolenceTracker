
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:template_app/style.dart';

class CustomText extends StatelessWidget {

  final String title;
  final bool isCenter;
  final bool isBold;
  final bool isPrimary;
  final double textSize;
  final Color textColor;

  const CustomText({Key key, this.title, this.isCenter = false, this.textSize = 15, this.isBold = false, this.isPrimary = false, this.textColor = appBlackColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _buildTitleText();
  }

  Widget _buildTitleText() {

    FontWeight _fontWeight ;
    TextStyle _textStyle;
    Color _txtColor;

    _fontWeight = isBold ? FontWeight.bold : FontWeight.normal;
    _textStyle = isPrimary ? textStylePrimaryDarkColorNormal : textStyleBlackColorNormal;
    _txtColor = isPrimary ? appPrimaryDarkColor : textColor;

    return AutoSizeText(
        title,
        textAlign: isCenter ? TextAlign.center : TextAlign.left,
        style: _textStyle.copyWith(fontSize: textSize, fontWeight: _fontWeight, color: _txtColor),
        wrapWords: true,
        softWrap: true
    );
  }
}