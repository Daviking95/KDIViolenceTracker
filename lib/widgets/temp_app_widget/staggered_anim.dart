

import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

Widget listViewAnim ({int itemCount, Widget childList}) {
  return AnimationLimiter(
    child: ListView.builder(
      itemCount: itemCount,
      itemBuilder: (BuildContext context, int index) {
        return AnimationConfiguration.staggeredList(
          position: index,
          duration: const Duration(milliseconds: 375),
          child: SlideAnimation(
            verticalOffset: 50.0,
            child: FadeInAnimation(
              child: childList,
            ),
          ),
        );
      },
    ),
  );
}


Widget gridViewAnim ({int itemCount, int columnCount, Widget childList}) {
  return AnimationLimiter(
    child: GridView.count(
      crossAxisCount: columnCount,
      children: List.generate(
        itemCount,
            (int index) {
          return AnimationConfiguration.staggeredGrid(
            position: index,
            duration: const Duration(milliseconds: 375),
            columnCount: columnCount,
            child: ScaleAnimation(
              child: FadeInAnimation(
                child: childList,
              ),
            ),
          );
        },
      ),
    ),
  );
}

//link to resources : https://github.com/mobiten/flutter_staggered_animations