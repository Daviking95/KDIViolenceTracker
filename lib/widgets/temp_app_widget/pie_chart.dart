import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

Widget pieChart(
    BuildContext context, Map<String, double> data, List<Color> colors) {
  return PieChart(
    dataMap: data,
    colorList: colors,
    // if not declared, random colors will be chosen
    animationDuration: Duration(milliseconds: 1500),
    chartLegendSpacing: 32.0,
    chartRadius: MediaQuery.of(context).size.width / 2.7,
    //determines the size of the chart
    showChartValuesInPercentage: true,
    showChartValues: true,
    showChartValuesOutside: false,
    chartValueBackgroundColor: Colors.grey[200],
    showLegends: true,
    legendPosition: LegendPosition.right,
    //can be changed to top, left, bottom
    decimalPlaces: 1,
    showChartValueLabel: true,
    initialAngle: 0,
    chartValueStyle: defaultChartValueStyle.copyWith(
      color: Colors.blueGrey[900].withOpacity(0.9),
    ),
    chartType: ChartType.disc, //can be changed to ChartType.ring
  );
}
