import 'package:flutter/material.dart';
import 'package:template_app/style.dart';

class AppTextInputs extends StatelessWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final int maxLength;
  final TextInputType textInputType;
  final Color color;
  final bool autoFocus;
  final Function validateInput;
  final Function onTapFunction;
  final Function onChange;
  final bool isReadOnly;
  final int maxLine;

  AppTextInputs(
      {this.textInputTitle,
      this.controller,
      this.maxLength = 0,
      this.textInputType,
      this.color = appWhiteColor,
      this.autoFocus = false,
      this.validateInput,
      this.onTapFunction,
      this.isReadOnly = false,
      this.onChange, this.maxLine = 1});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 15.0,
          ),
          maxLength == 0
              ? materialWrapper(_buildTextInput())
              : materialWrapper(_buildTextInputWithMaxLength()),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }

  Widget _buildTextInput() {
    return TextFormField(
      decoration: textInputDecorator(textInputTitle, color),
      style: textStyleWhiteColorNormal.copyWith(color: color),
      cursorColor: color,
      keyboardType: textInputType,
      controller: controller,
      autofocus: autoFocus,
      validator: validateInput,
      onTap: onTapFunction,
      readOnly: isReadOnly,
      onChanged: onChange,
      maxLines: maxLine,
    );
  }

  Widget _buildTextInputWithMaxLength() {
    return TextFormField(
      decoration: textInputDecorator(textInputTitle, color),
      style: textStyleWhiteColorNormal.copyWith(color: color),
      cursorColor: color,
      keyboardType: textInputType,
      maxLength: maxLength,
      maxLengthEnforced: true,
      controller: controller,
      validator: validateInput,
      onTap: onTapFunction,
      readOnly: isReadOnly,
      onChanged: onChange,
      maxLines: maxLine,
    );
  }
}

class AppPasswordTextInput extends StatefulWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final Function validateInput;
  final Color color;

  AppPasswordTextInput(
      {this.textInputTitle, this.controller, this.validateInput, this.color});

  @override
  _AppPasswordTextInputState createState() => _AppPasswordTextInputState();
}

class _AppPasswordTextInputState extends State<AppPasswordTextInput> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 15.0,
        ),
        materialWrapper(
          TextFormField(
            obscureText: _obscureText,
            keyboardType: TextInputType.visiblePassword,
            style: textStyleWhiteColorNormal.copyWith(color: widget.color),
            validator: widget.validateInput,
            controller: widget.controller,
            decoration: textInputDecorator(widget.textInputTitle, widget.color)
                .copyWith(
              suffixIcon: FlatButton(
                onPressed: _toggle,
                child: new Text(
                  _obscureText ? "Show" : "Hide",
                  style: textStyleSecondaryColorNormal,
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
      ],
    );
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
}

InputDecoration textInputDecorator(String text, Color color) {
  return InputDecoration(
      counterStyle: textStyleWhiteColorNormal.copyWith(color: color),
      labelText: text,
      fillColor: Color(0XFFF0F4F7),
//    labelStyle: textStyleWhiteColorNormal.copyWith(color: color),
      filled: true,
      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(color: Color(0XFFF0F4F7), width: 3.0))
//    focusedBorder: OutlineInputBorder(
//      borderSide: BorderSide(color: color, width: 1.0),
//    ),
//    enabledBorder: OutlineInputBorder(
//      borderSide: BorderSide(color: color, width: 1.0),
//    ),
      );
}

Widget materialWrapper(Widget widget) {
  return Material(elevation: 10.0, shadowColor: Colors.blue, child: widget);
}
