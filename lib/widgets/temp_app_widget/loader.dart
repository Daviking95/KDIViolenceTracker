import 'package:flutter/material.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class AppLoader extends StatefulWidget {
  final BuildContext context;
  final String title;
  final String successTitle;
  final String successSubTitle;
  final String routeToMoveTo;
  final bool doesRouteHasArgument;
  final Function routeWithArgument;
  final Function httpRequestsToRun;

  AppLoader({
    this.context,
    this.title,
    this.routeToMoveTo = '',
    this.successSubTitle,
    this.successTitle = '',
    this.routeWithArgument,
    this.doesRouteHasArgument = false,
    this.httpRequestsToRun,
  });

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AppLoaderState();
  }
}

class AppLoaderState extends State<AppLoader>
    with SingleTickerProviderStateMixin {
  bool isLoadingSuccessful = false;
  AnimationController _controller;
  Animation<Offset> _offsetAnimation;
  HttpResponseMessage registerUserResponse = new HttpResponseMessage();

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat(reverse: true);

    _offsetAnimation = Tween<Offset>(
      begin: const Offset(0.0, -1.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInCirc,
    ));

    setState(() {
      isLoadingSuccessful = true;
    });
  }

//  @override
//  void didUpdateWidget(AppLoader oldWidget) {
//    super.didUpdateWidget(oldWidget);
//  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFffffff),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FutureBuilder(
              future: widget.httpRequestsToRun(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.none &&
                    snapshot.hasData == null) {
                  alertDialogWithText(
                      context: context,
                      title: "Error Processing Request",
                      desc:
                      "Sorry, this request cannot be proccessed at this time. Ensure you have data connection");

                  return null;
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: SlideTransition(
                            position: _offsetAnimation,
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Image.asset(
                                ImageStringConstants.imgAppFavLogo,
                                width: 40.0,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        CustomText(
                          title: widget.title,
                          isCenter: true,
                        )
                      ],
                    ),
                  );
                } else if (snapshot.hasError) {
                  alertDialogWithText(
                      context: context,
                      title: "Error Processing Request",
                      desc:
                      "Sorry, this request cannot be proccessed at this time. ${HttpResponseMessage.message}");
                  return null;
                } else if (snapshot.hasData) {
                  if (snapshot.data == false) {
                    return _errorWidget();
                  }

                  _handleTimeout();

                  return Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            ImageStringConstants.imgVtLogo,
                            width: 250.0,
//                        color: Colors.green,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          CustomText(
                            title: widget.successTitle,
                            isBold: true,
                            textSize: 25.0,
                            isCenter: true,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          CustomText(
                            title: widget.successSubTitle,
                            isCenter: true,
                            textSize: 20.0,
                          )
                        ],
                      ),
                    ),
                  );
                } else {
                  alertDialogWithText(
                      context: context,
                      title: "Error Processing Request",
                      desc:
                      "Sorry, this request cannot be proccessed at this time. Ensure you have data connection");
                  return null;
                }
              },
            )
          ],
        ),
      ),
    );
  }

  _handleTimeout() {
    if (mounted) {
      Future.delayed(const Duration(milliseconds: 3000), () {
        if (widget.doesRouteHasArgument)
          return widget.routeWithArgument();
        else
          return Navigator.pushReplacementNamed(context, widget.routeToMoveTo);
      });
    }
  }

  Widget _errorWidget() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.error,
              size: 80.0,
              color: appActiveColor,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              HttpResponseMessage.message = HttpResponseMessage.message ==
                  null ||
                  HttpResponseMessage.message.isEmpty
                  ? "Cannot proccess request. Please ensure your data is correct and there is internet connection "
                  : HttpResponseMessage.message,
              style: textStylePrimaryColorBold.copyWith(
                  fontSize: 18.0, color: appActiveColor),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 50.0,
            ),
            AppPrimaryDarkButtonRound(
              context: context,
              textTitle: "OKAY",
              width: getTargetWidth(context),
              bgColor: appPrimaryDarkColor,
              functionToRun: _popPage,
              borderRadius: 50.0,
              arguments: false,
              textStyle: textStyleWhiteColorNormal,
            ),
          ],
        ),
      ),
    );
  }

  _popPage() {
    Navigator.pop(context);
  }
}
