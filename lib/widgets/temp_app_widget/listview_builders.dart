
import 'package:flutter/material.dart';

customListView(BuildContext context, int itemCount, Widget childWidget){
  return ListView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      addAutomaticKeepAlives: true,
      itemCount: itemCount,
      itemBuilder: (context, index) {
        return childWidget;
      });
}