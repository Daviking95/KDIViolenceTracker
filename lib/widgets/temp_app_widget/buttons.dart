import 'package:flutter/material.dart';
import 'package:template_app/helpers/route_helper.dart';

class AppPrimaryDarkButtonRound extends StatelessWidget {
  final String textTitle;
  final String routeToGo;
  final BuildContext context;
  final Color bgColor;
  final TextStyle textStyle;
  final double width;
  final double borderRadius;
  final Object arguments;
  final Function functionToRun;

  const AppPrimaryDarkButtonRound(
      {Key key,
      this.context,
      this.textTitle,
      this.routeToGo,
      this.bgColor,
      this.textStyle,
      this.width = 0,
      this.arguments = true,
      this.functionToRun,
      this.borderRadius = 20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return AnimatedContainer(
      duration: Duration(seconds: 5),
      curve: Curves.bounceIn,
      child: Material(
        elevation: 3.0,
        borderRadius: BorderRadius.circular(borderRadius),
        color: bgColor,
        child: MaterialButton(
          minWidth: width == 0 ? 0 : width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            routeToGo != null
                ? RouteHelpers.navigateRoute(context:context, routeName :routeToGo, arguments: arguments, routeType : 1)
                : functionToRun();
          },
          child: Text(
            textTitle,
            textAlign: TextAlign.center,
            style: textStyle,
          ),
        ),
      ),
    );
  }
}

class AppOutlineButton extends StatelessWidget {
  final double borderRadius;
  final BuildContext context;
  final double width;
  final Color borderColor;
  final TextStyle textStyle;
  final String textTitle;
  final String routeToGo;
  final Object arguments;
  final Function functionToRun;

  AppOutlineButton(
      {this.borderRadius = 20.0,
      this.context,
      this.width = 0,
      this.borderColor,
      this.textTitle,
      this.routeToGo,
      this.arguments = true,
      this.functionToRun,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: width == 0 ? 0 : width,
      child: OutlineButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(borderRadius),
        ),
        borderSide: BorderSide(
          color: borderColor,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () => routeToGo != null
            ? RouteHelpers.navigateRoute(context:context, routeName :routeToGo, arguments: arguments, routeType : 1)
            : functionToRun(),
        child: Text(
          textTitle,
          textAlign: TextAlign.center,
          style: textStyle,
        ),
      ),
    );
  }
}
