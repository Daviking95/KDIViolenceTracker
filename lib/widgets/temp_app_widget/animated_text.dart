
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/style.dart';

Widget rotateText ({String staticText, String textStream1, String textStream2, String textStream3, Function onTapFunc, double sbHeight, double sbWidth}) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      SizedBox(width: sbWidth, height: sbHeight),
      Text(
        staticText,
        style: TextStyle(fontSize: 43.0),
      ),
      SizedBox(width: sbWidth, height: sbHeight),
      RotateAnimatedTextKit(
          onTap: () {
            onTapFunc();
          },
          text: [textStream1, textStream2, textStream3],
          textStyle: TextStyle(fontSize: 40.0, fontFamily: AppConstants.appFontFamily),
          textAlign: TextAlign.start,
          alignment: AlignmentDirectional.topStart // or Alignment.topLeft
      ),
    ],
  );

}

Widget fadeText ({String staticText, String textStream1, String textStream2, String textStream3, Function onTapFunc, double sbHeight, double sbWidth}) {
  return SizedBox(
    width: sbWidth,
    child: FadeAnimatedTextKit(
        onTap: () {
          onTapFunc();
        },
        text: [
          textStream1,
          textStream2,
          textStream3
        ],
        textStyle: TextStyle(
            fontSize: 32.0,
            fontWeight: FontWeight.bold
        ),
        textAlign: TextAlign.start,
        alignment: AlignmentDirectional.topStart // or Alignment.topLeft
    ),
  );

}

Widget typeWriterText ({String staticText, String textStream1, String textStream2, String textStream3, Function onTapFunc, double sbHeight, double sbWidth}) {
  return SizedBox(
    width: sbWidth,
    child: TypewriterAnimatedTextKit(
        onTap: () {
          onTapFunc();
        },
        text: [
          textStream1,
          textStream2,
          textStream3,
        ],
        textStyle: TextStyle(
            fontSize: 30.0,
            fontFamily: AppConstants.appFontFamily
        ),
        textAlign: TextAlign.start,
        alignment: AlignmentDirectional.topStart // or Alignment.topLeft
    ),
  );

}

Widget scaleText ({String staticText, String textStream1, String textStream2, String textStream3, Function onTapFunc, double sbHeight, double sbWidth}) {
  return SizedBox(
    width: sbWidth,
    child: ScaleAnimatedTextKit(
        onTap: () {
          onTapFunc();
        },
        text: [
          textStream1,
          textStream2,
          textStream3
        ],
        textStyle: TextStyle(
            fontSize: 70.0,
            fontFamily: AppConstants.appFontFamily
        ),
        textAlign: TextAlign.start,
        alignment: AlignmentDirectional.topStart // or Alignment.topLeft
    ),
  );

}

Widget colorizeText ({String staticText, String textStream1, String textStream2, String textStream3, Function onTapFunc, double sbHeight, double sbWidth}) {
  return SizedBox(
    width: sbWidth,
    child: ColorizeAnimatedTextKit(
        onTap: () {
          onTapFunc();
        },
        text: [
          textStream1,
          textStream2,
          textStream3,
        ],
        textStyle: TextStyle(
            fontSize: 50.0,
            fontFamily: AppConstants.appFontFamily
        ),
        colors: [
          appPrimaryColor,
          appBrownGrayOneColor,
          appSecondaryColor,
          appPrimaryDarkColor,
        ],
        textAlign: TextAlign.start,
        alignment: AlignmentDirectional.topStart // or Alignment.topLeft
    ),
  );

}

Widget textLiquidFillText ({String staticText, Color waveColor, Color boxBgColor, double sbHeight, double sbWidth}) {
  return SizedBox(
    width: sbWidth,
    child: TextLiquidFill(
      text: staticText,
      waveColor: waveColor,
      boxBackgroundColor: boxBgColor,
      textStyle: TextStyle(
        fontSize: 80.0,
        fontWeight: FontWeight.bold,
      ),
      boxHeight: sbHeight,
    ),
  );

}

// link to this resource : https://github.com/aagarwal1012/Animated-Text-Kit