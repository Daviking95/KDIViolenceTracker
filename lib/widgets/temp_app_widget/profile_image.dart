
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/style.dart';

Widget profileImage(){
  return CircleAvatar(
    backgroundColor: appPrimaryDarkColor,
    child: Icon(
      LineIcons.user,
      color: appWhiteTwoColor,
    ),
  );
}