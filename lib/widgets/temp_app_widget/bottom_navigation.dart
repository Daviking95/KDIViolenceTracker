import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/screens/reports/add_report.dart';
import 'package:template_app/style.dart';

class FancyBottomNav extends StatefulWidget {
  final int currentPage;

  FancyBottomNav({this.currentPage});

  @override
  _FancyBottomNavState createState() => _FancyBottomNavState();
}

class _FancyBottomNavState extends State<FancyBottomNav> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return FancyBottomNavigation(
      tabs: [
        TabData(iconData: Icons.home, title: "Home"),
        TabData(iconData: Icons.add, title: "Add Report"),
        TabData(iconData: Icons.menu, title: "Profile")
      ],
      onTabChangedListener: (position) {
        setState(() {
          currentPage = position;
        });
      },
    );
  }
}

class GBBottomNavigation extends StatefulWidget {
  final int selectedIndex;

  GBBottomNavigation({this.selectedIndex});

  @override
  _GBBottomNavigationState createState() => _GBBottomNavigationState();
}

class _GBBottomNavigationState extends State<GBBottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
      ]),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
          child: GNav(
            gap: 8,
            activeColor: Colors.white,
            iconSize: 24,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            duration: Duration(milliseconds: 500),
            tabBackgroundColor: appSecondaryColor,
            tabs: [
              GButton(
                icon: LineIcons.home,
                text: 'Home',
              ),
              GButton(
                icon: LineIcons.plus,
                text: 'Report',
//                onPressed: _addReport,
              ),
              GButton(
                icon: LineIcons.play,
                text: 'Media',
              ),
              GButton(
                icon: LineIcons.user,
                text: 'Profile',
              ),
            ],
            selectedIndex: widget.selectedIndex,
            onTabChange: (index) {
              if (mounted) {
                if (index == 0) {
                  Navigator.pushReplacementNamed(
                      context, RoutesConstants.dashboardUrl);
                } else if (index == 1) {
                  RouteHelpers.navigateRoute(context: context, widgetRoute: AddReportScreen(currentStep: 0,), routeType: 7);
                }else if (index == 2) {
                  Navigator.pushReplacementNamed(
                      context, RoutesConstants.mediaUrl);
                }else if (index == 3) {
                  Navigator.pushReplacementNamed(
                      context, RoutesConstants.profileUrl);
                } else {
                  Navigator.pushReplacementNamed(
                      context, RoutesConstants.starterUrl);
                }
              }
            },
          ),
        ),
      ),
    );
  }
}

class CustomBottomNavigation extends StatefulWidget {
  final int selectedIndex;

  CustomBottomNavigation({this.selectedIndex});

  @override
  _CustomBottomNavigationState createState() => _CustomBottomNavigationState();
}

class _CustomBottomNavigationState extends State<CustomBottomNavigation> {
  double iconSize = 18.0;

  void _onItemTapped(int index) {
    setState(() {
      if (index == 0) {
        Navigator.pushReplacementNamed(context, RoutesConstants.starterUrl);
      } else if (index == 1) {
        Navigator.pushReplacementNamed(context, RoutesConstants.starterUrl);
      } else {
        Navigator.pushReplacementNamed(context, RoutesConstants.starterUrl);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BottomNavigationBar(
      elevation: 10.0,
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: appBrownGrayTwoColor,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          activeIcon: Icon(Icons.home),
          icon: Icon(Icons.home),
          title: Text("Home"),
        ),
        BottomNavigationBarItem(
          activeIcon: Icon(Icons.person),
          icon: Icon(Icons.person),
          title: Text("Account"),
        ),
      ],
      currentIndex: widget.selectedIndex,
      selectedItemColor: appPrimaryDarkColor,
      onTap: _onItemTapped,
    );
  }
}
