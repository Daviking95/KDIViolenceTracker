import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

cardWithRadius(
    {Widget childWidget, Function function, double elevation = 7.0}) {
  return InkWell(
    onTap: () => function,
    splashColor: appBrownGrayOneColor,
    focusColor: appBrownGrayOneColor,
    child: Card(
        elevation: elevation,
        shadowColor: appPrimaryDarkColor.withOpacity(.8),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: childWidget),
  );
}

Widget savedFormCardWidget({bool isFormSubmitted}) {
  return Container(
    child: cardWithRadius(
      elevation: 3.0,
      childWidget: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            CustomText(
              title: isFormSubmitted ? "Complete" : "Incomplete",
              textSize: 12.0,
              textColor: isFormSubmitted ? Colors.green : Colors.red,
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CustomText(
                        title: "3 Party A Supporters Wounded in Clashes with...",
                        isBold: true,
                        textSize: 15.0,
                        textColor: appBlackTwoColor,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: CustomText(
                    title: "12:43 PM",
                    textSize: 10.0,
                    textColor: appBlackColor,
                  ),
                ),
                Expanded(
                  child: CustomText(
                    title: "06/06/2020",
                    textSize: 10.0,
                    textColor: appBlackColor,
                  ),
                ),
                isFormSubmitted ? Container() : GestureDetector(
//                onTap: () => _downloadStatement("0000011111"),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: appSecondaryColor),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 35.0, vertical: 10.0),
                      child: Text(
                        "Send",
                        style: textStyleWhiteColorNormal,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
    ),
  );
}


Widget galleryCardWidget({bool isFormSubmitted}) {
  return Container(
    child: cardWithRadius(
      elevation: 3.0,
      childWidget: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            CustomText(
              title: isFormSubmitted ? "Sent" : "Not Sent",
              textSize: 12.0,
              textColor: isFormSubmitted ? Colors.green : Colors.red,
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: CustomText(
                    title: "3 Party A Supporters Wounded in Clashes with...",
                    isBold: true,
                    textSize: 15.0,
                    textColor: appBlackTwoColor,
                  ),
                ),
                Expanded(child: Column(
                  children: <Widget>[
                    CustomText(
                      title: "Click to play",
                      isBold: true,
                      textSize: 12.0,
                      textColor: appBlackTwoColor,
                    ),
                    Icon(LineIcons.play, size: 30.0,),
                  ],
                )),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Center(
              child: GFAvatar(
                backgroundImage:AssetImage(ImageStringConstants.imgAppEllipseSecondary),
                shape: GFAvatarShape.standard,
                size: 100,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: CustomText(
                    title: "12:43 PM",
                    textSize: 10.0,
                    textColor: appBlackColor,
                  ),
                ),
                Expanded(
                  child: CustomText(
                    title: "06/06/2020",
                    textSize: 10.0,
                    textColor: appBlackColor,
                  ),
                ),
                isFormSubmitted ? Container() : GestureDetector(
//                onTap: () => _downloadStatement("0000011111"),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: appSecondaryColor),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 35.0, vertical: 10.0),
                      child: Text(
                        "Send",
                        style: textStyleWhiteColorNormal,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
    ),
  );
}

Widget recordingCardWidget({bool isFormSubmitted}) {
  return Container(
    child: cardWithRadius(
      elevation: 3.0,
      childWidget: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            CustomText(
              title: isFormSubmitted ? "Sent" : "Not Sent",
              textSize: 12.0,
              textColor: isFormSubmitted ? Colors.green : Colors.red,
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: CustomText(
                    title: "3 Party A Supporters Wounded in Clashes with...",
                    isBold: true,
                    textSize: 15.0,
                    textColor: appBlackTwoColor,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Center(
              child: Column(
                children: <Widget>[
                  CustomText(
                    title: "Click to play",
                    isBold: true,
                    textSize: 12.0,
                    textColor: appBlackTwoColor,
                  ),
                  Icon(LineIcons.play, size: 30.0,),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: CustomText(
                    title: "12:43 PM",
                    textSize: 10.0,
                    textColor: appBlackColor,
                  ),
                ),
                Expanded(
                  child: CustomText(
                    title: "06/06/2020",
                    textSize: 10.0,
                    textColor: appBlackColor,
                  ),
                ),
                isFormSubmitted ? Container() : GestureDetector(
//                onTap: () => _downloadStatement("0000011111"),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: appSecondaryColor),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 35.0, vertical: 10.0),
                      child: Text(
                        "Send",
                        style: textStyleWhiteColorNormal,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
    ),
  );
}
