
import 'package:flutter/material.dart';
import 'package:template_app/enums.dart';
import 'package:template_app/style.dart';

Widget customErrorMessage(String msg, FieldError error) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      SizedBox(height: 5),
      Text(
        _errorText(error, msg),
        style: TextStyle(color: appActiveColor),
      ),
    ],
  );
}

String _errorText(FieldError error, String msg) {
  switch (error) {
    case FieldError.Empty:
      return 'You need to enter $msg';
    case FieldError.Invalid:
      return '$msg invalid';
    default:
      return '';
  }
}
