
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

customNavDrawer(BuildContext context) {

  return Drawer(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
        children: [
      Container(
        margin: EdgeInsets.only(top: 10.0),
        padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0 ),
        child: CustomText(
          title: "Global Industries",
          isBold: true,
        ),
      ),
      Divider(height: 1.0, color: appBrownGrayOneColor,),
      ListTile(
        title: navItem(LineIcons.hourglass_1, "Get Started"),
        onTap: () {
          RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
        },
      ),
      ListTile(
        title: navItem(LineIcons.dashboard, "Dashboard"),
        onTap: () {
          RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
        },
      ),
      ListTile(
        title: navItem(Icons.credit_card, "Payments"),
        onTap: () {
          RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
        },
      ),
      ListTile(
        title: navItem(Icons.supervisor_account, "Accounts"),
        onTap: () {
          RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
        },
      ),
      ListTile(
        title: navItem(Icons.swap_horiz, "Transfers"),
        onTap: () {
          RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
        },
      ),
//      ListTile(
//        title: navItem(Icons.assignment, "Cheques"),
//        onTap: () {
//          RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
//        },
//      ),
      Expanded(
        flex: 2,
        child: Container(
          margin: EdgeInsets.only(bottom: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              ListTile(
                title: navItem(Icons.settings, "Settings"),
                onTap: () {
                  RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
                },
              ),
              ListTile(
                title: navItem(Icons.report, "Report"),
                onTap: () {
                  RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.dashboardUrl, routeType: 8);
                },
              ),
              ListTile(
                title: navItem(LineIcons.sign_out, "Signout"),
                onTap: () {
                  RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.loginUrl, routeType: 8);
                },
              ),
            ],
          ),
        ),
      )
    ]),
  );
}

Widget navItem(IconData icon, String title){
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      children: <Widget>[
        CircleAvatar(
          backgroundColor: appBackgroundColor,
            foregroundColor: appBlackColor,
            child: Icon(icon)),
        SizedBox(width: 20.0,),
        CustomText(title: title,)
      ],
    ),
  );
}