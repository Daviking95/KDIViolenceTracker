
import 'package:flutter/material.dart';
import 'package:template_app/style.dart';

Widget startFabWidget(Function function, Color fabBgColor, [IconData icon = Icons.add]) {
  return FloatingActionButton(
    onPressed: () => function(),
    elevation: 5.0,
    child: Icon(
      icon,
      color: appWhiteColor,
      size: 40.0,
    ),
    backgroundColor: fabBgColor,
  );
}

class FancyFab extends StatefulWidget {
  final Function() onPressedFunc1, onPressedFunc2, onPressedFunc3;
  final IconData icon1, icon2, icon3;

  FancyFab({this.onPressedFunc1, this.onPressedFunc2, this.onPressedFunc3, this.icon1, this.icon2, this.icon3});

  @override
  _FancyFabState createState() => _FancyFabState();
}

class _FancyFabState extends State<FancyFab>
    with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 56.0;

  @override
  initState() {
    _animationController =
    AnimationController(vsync: this, duration: Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {});
      });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _buttonColor = ColorTween(
      begin: Colors.blue,
      end: Colors.red,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));
    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: -14.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget widget1(IconData icon, Function function) {
    return Container(
      child: FloatingActionButton(
        onPressed: function(),
        child: Icon(icon),
      ),
    );
  }

  Widget widget2(IconData icon, Function function) {
    return Container(
      child: FloatingActionButton(
        onPressed: function(),
        child: Icon(icon),
      ),
    );
  }

  Widget widget3(IconData icon, Function function) {
    return Container(
      child: FloatingActionButton(
        onPressed: function(),
        child: Icon(icon),
      ),
    );
  }

  Widget toggle() {
    return Container(
      child: FloatingActionButton(
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: 'Toggle',
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 3.0,
            0.0,
          ),
          child: widget1(widget.icon1, widget.onPressedFunc1),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value * 2.0,
            0.0,
          ),
          child: widget2(widget.icon1, widget.onPressedFunc1),
        ),
        Transform(
          transform: Matrix4.translationValues(
            0.0,
            _translateButton.value,
            0.0,
          ),
          child: widget3(widget.icon1, widget.onPressedFunc1),
        ),
        toggle(),
      ],
    );
  }
}