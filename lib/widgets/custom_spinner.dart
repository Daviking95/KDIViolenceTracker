import 'package:flutter/material.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class AppSpinner extends StatefulWidget {
  final List<String> _spinnerList;
  final double borderRadius;
  final String hint;
  final TextEditingController controller;
  String result;

  AppSpinner(this._spinnerList, this.borderRadius, this.hint, this.controller,
      [this.result]);

  @override
  _AppSpinnerState createState() => _AppSpinnerState();
}

class _AppSpinnerState extends State<AppSpinner> {
  String dropdownValueForAccountSelected;

  @override
  Widget build(BuildContext context) {

    widget.result = widget.controller.text.isEmpty ? null : widget.controller.text;

    return materialWrapper(
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Container(
          decoration: BoxDecoration(
              color: Color(0XFFF0F4F7),
//            border: Border.all(color: appBrownGrayOneColor.withOpacity(.3), width: 1.0),
              borderRadius: BorderRadius.circular(widget.borderRadius)),
          child: SizedBox(
            width: double.infinity,
            height: 35.0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: Color(0XFFF0F4F5),
                ),
                child: DropdownButton<String>(
                  value: widget.result,
                  hint: CustomText(
                    title: widget.hint,
                    textColor: Colors.black,
                  ),
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  style: textStyleSecondaryColorNormal,
                  onChanged: (String data) {
                    setState(() {
                      dropdownValueForAccountSelected = data;
                      _returnValueString(data);
                    });
                  },
                  isExpanded: true,
                  items: widget._spinnerList
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: textStylePrimaryColorNormal,
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  TextEditingController _returnValueString(String data) {
    widget.result = data;
    widget.controller.text = data;
    widget.result = widget.controller.text;
    print("Data coming ${widget.result}");
    return widget.controller;
  }
}
