import 'package:template_app/app_constants.dart';
import 'package:template_app/model/http_model/login.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<dynamic> loginUserRequest() async {

  return true;
  try {
    final Map<String, String> _loginData = {
      "email": LoginUser.email,
      "password": LoginUser.password,
    };

    var requestBody = json.encode(_loginData);

    print(requestBody);

    var responseBody;

    http.Response response = await http.post(
      EndpointsConstants.loginStUrl,
      body: requestBody,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
    );

    responseBody = json.decode(response.body);

    print(responseBody);

    if (response.statusCode != 200) {
      HttpResponseMessage.message = responseBody;
      return false;
    }

    AppConstants.isUserAuthenticated = true;

//    HttpDataConstants.loggedInUserData =
//        LoggedInUserData.fromJson(responseBody);

    HttpResponseMessage.message = HttpDataConstants.loggedInUserData.message;
    return true;
  } catch (e) {
    print("Coming from login : $e");
    HttpResponseMessage.message = "Error occured. Please try again";
    return false;
  }
}
