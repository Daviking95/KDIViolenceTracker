import 'package:flutter/material.dart';
import 'package:template_app/screens/dashboard/main_dashboard.dart';
import 'package:template_app/screens/media/main_media.dart';
import 'package:template_app/screens/onboarding/change_password.dart';
import 'package:template_app/screens/onboarding/forgot_password.dart';
import 'package:template_app/screens/onboarding/login.dart';
import 'package:template_app/screens/onboarding/otp.dart';
import 'package:template_app/screens/onboarding/register.dart';
import 'package:template_app/screens/profile/main_profile.dart';
import 'package:template_app/screens/reports/add_report.dart';
import 'package:template_app/screens/start_screen.dart';
import 'package:template_app/style.dart';

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.name == "/") return child;
    if (animation.status == AnimationStatus.reverse)
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);
    return FadeTransition(opacity: animation, child: child);
  }
}

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {

    final args = settings.arguments;

    print(args);


    switch (settings.name) {
      case '/':
        return new MyCustomRoute(
          builder: (_) => new StarterScreen(),
          settings: settings,
        );
      case '/dashboard':
        return new MyCustomRoute(
          builder: (_) => new DashboardScreen(),
          settings: settings,
        );
      case '/profile':
        return new MyCustomRoute(
          builder: (_) => new ProfileScreen(),
          settings: settings,
        );
      case '/login':
        return new MyCustomRoute(
          builder: (_) => new LoginScreen(),
          settings: settings,
        );
      case '/register':
        return new MyCustomRoute(
          builder: (_) => new RegisterScreen(),
          settings: settings,
        );
      case '/forgot_password':
        return new MyCustomRoute(
          builder: (_) => new ForgotPasswordScreen(),
          settings: settings,
        );
      case '/change_password':
        return new MyCustomRoute(
          builder: (_) => new ChangePasswordScreen(),
          settings: settings,
        );
      case '/otp':
        return new MyCustomRoute(
          builder: (_) => new OtpScreen(),
          settings: settings,
        );
      case '/add_report':
        return new MyCustomRoute(
          builder: (_) => new AddReportScreen(),
          settings: settings,
        );
      case '/profile':
        return new MyCustomRoute(
          builder: (_) => new ProfileScreen(),
          settings: settings,
        );
      case '/media':
        return new MyCustomRoute(
          builder: (_) => new MediaScreen(),
          settings: settings,
        );
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: appPrimaryDarkColor,
          title: Text('Error'),
        ),
        body: Center(
          child: Text('Error : Page Not Found', style: textStylePrimaryDarkColorBold,),
        ),
      );
    });
  }
}

