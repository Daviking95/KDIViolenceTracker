
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

class MoneyFormatter {

  String returnFormattedMoney(double value, String currencySymbol){

    MoneyFormatterOutput fmf = new FlutterMoneyFormatter(
        amount: value,
        settings: MoneyFormatterSettings(
            symbol: currencySymbol,
            thousandSeparator: ',',
            decimalSeparator: '.',
            symbolAndNumberSeparator: '',
            fractionDigits: 2,
            compactFormatType: CompactFormatType.short
        )
    ).output;

    String val = value.toString();
    return val.length < 9 ? fmf.symbolOnLeft : fmf.compactSymbolOnLeft;
  }
}