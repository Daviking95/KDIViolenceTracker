import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/toasts_and_snacks.dart';

class Validators {

  bool isFieldEmpty(String fieldValue) => fieldValue?.isEmpty ?? true;

  String validateName(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[A-za-z ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  String validateEmail(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim()))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.trim().isEmpty) return 'Please choose a password.';
    if (value.trim().length < 6) return 'Password must be of at least 6 lengths.';
//    Pattern pattern =
//        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~])$';
//    RegExp regex = new RegExp(pattern);
//    if (!regex.hasMatch(value.trim()))
//      return 'Password should contain at least one upper case, \nat least one lower case, \nat least one digit and at least one Special character.';
//    else
    return null;
  }


  String validateNumber(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';
    if (value.trim().length != 11)
      return 'Field must be of 11 digits';
    else
      return null;
  }

  String validatePhoneNumber(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^(?:[+0])?[0-9]{10}$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only correct phone number.';
    if (value.trim().length != 11)
      return 'Field must be of 11 digits';
    else
      return null;
  }

  String validateString(dynamic value) {
    if (value.toString().trim().isEmpty)
      return 'Field is required.';
    else
      return null;
  }

  bool spinnerVaidation(TextEditingController controller, String text) {
    if (controller.text.isEmpty) {
      showToast(
          toastMsg: text,
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.BOTTOM,
          bgColor: appPrimaryDarkColor,
          txtColor: appWhiteColor);
      return false;
    }else {
      return true;
    }
  }

}
