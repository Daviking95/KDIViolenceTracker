import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:template_app/helpers/shared_pref_helper.dart';
import 'package:template_app/model/app_model/app_model.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';

getCurrentLocation() {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;

  geolocator
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
      .then((Position position) {
    _currentPosition = position;
    AppModel.userCurrentLatitude = _currentPosition.latitude;
    AppModel.userCurrentLongitude = _currentPosition.longitude;

    print(
        "LAT: ${_currentPosition.latitude}, LNG: ${_currentPosition.longitude}");

//    getAddressFromLatLng();
    getUserLocation(_currentPosition.latitude, _currentPosition.longitude);
  }).catchError((e) {
    print(e);
  });
}

getAddressFromLatLng() async {
  final Geolocator geolocator = Geolocator()..getCurrentPosition();
  String _currentAddress;

  try {
    List<Placemark> p = await geolocator.placemarkFromCoordinates(
        AppModel.userCurrentLatitude, AppModel.userCurrentLongitude);

    Placemark place = p[0];

    _currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";

    await saveToPref("currentLocation", _currentAddress);

    AppModel.userCurrentAddress = await getFromPref("currentLocation");

    print("Current address is $_currentAddress");
  } catch (e) {
    print(e);
  }
}

Future<String> getUserLocation(double latitude, double longitude) async {
  try {
    final coordinates = new Coordinates(latitude, longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    await saveToPref("currentLocation", first.addressLine);

    var dta = await getFromPref("currentLocation");
    print("Location ${dta}");

    AppModel.userCurrentAddress = '${first.addressLine}';

    print("Current address is ${AppModel.userCurrentAddress}");

    return first.addressLine;
  } catch (e) {
    print(e);
    return null;
  }
}

Future<String> requestLocationPermission(BuildContext context) async {
  try {
    String currentAddress = "";
    GeolocationStatus geolocationStatus =
    await Geolocator().checkGeolocationPermissionStatus();
    bool isLocationServiceEnabled =
    await Geolocator().isLocationServiceEnabled();

    if (geolocationStatus == GeolocationStatus.denied ||
        geolocationStatus == GeolocationStatus.disabled || !isLocationServiceEnabled) {
      alertDialogWithText(
          context: context, title: "Please set your location");
      return null;
    }

    Position currentPosition = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    currentAddress = await getUserLocation(
        currentPosition.latitude, currentPosition.longitude);

    return currentAddress;
  } catch (e) {
    print(e);
    return null;
  }
}
