

import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:template_app/model/app_model/device_info.dart';

Future<DeviceInfo> getDeviceInfo() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  try {
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;

      DeviceInfo.deviceModel = androidDeviceInfo.model;
      DeviceInfo.deviceIP = androidDeviceInfo.id;
      DeviceInfo.deviceManufacturer = androidDeviceInfo.manufacturer;
      DeviceInfo.deviceName = androidDeviceInfo.brand;
      DeviceInfo.deviceType = androidDeviceInfo.type;
      DeviceInfo.iMEI = androidDeviceInfo.androidId;

    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;

      DeviceInfo.deviceModel = iosInfo.model;
      DeviceInfo.deviceIP = iosInfo.identifierForVendor;
      DeviceInfo.deviceManufacturer = iosInfo.utsname.machine;
      DeviceInfo.deviceName = iosInfo.name;
      DeviceInfo.deviceType = iosInfo.utsname.machine;
      DeviceInfo.iMEI = iosInfo.identifierForVendor;
    }
  } on PlatformException {
    DeviceInfo.deviceModel = "";
    DeviceInfo.deviceIP = "";
    DeviceInfo.deviceManufacturer = "";
    DeviceInfo.deviceName = "";
    DeviceInfo.deviceType = "";
    DeviceInfo.iMEI = "";
  }
}