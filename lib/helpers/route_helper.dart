
import 'package:flutter/material.dart';
import 'package:template_app/app_constants.dart';

class RouteHelpers{

  static navigateRoute({BuildContext context, String routeName, Object arguments, int routeType, Route route, Widget widgetRoute}){

    switch (routeType){
      case 1 :
        Navigator.pushNamed(context, routeName, arguments: arguments);
      break;
      case 2:
        Navigator.pop(context);
      break;
      case 3:
        Navigator.popAndPushNamed(context, routeName, arguments: arguments);
        break;
      case 4:
        Navigator.pushNamedAndRemoveUntil(context, routeName, (route) => false, arguments: arguments);
        break;
      case 5:
        Navigator.pushReplacementNamed(context, routeName, arguments: arguments);
        break;
      case 6:
        Navigator.popUntil(context, (route) => false);
        break;
      case 7:
        Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) => widgetRoute));
        break;
      case 8 :
        Navigator.pushNamed(context, routeName);
        break;
      default:
        Navigator.pushNamed(context, routeName, arguments: arguments);
    }

  }
}
