import 'package:shared_preferences/shared_preferences.dart';
import 'package:template_app/app_constants.dart';

Future<bool> readFromPref(String key) async {
  try {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getString(key) ?? null;

    return value == null ? false : true;
  } catch (e) {
    return false;
  }
}

Future<dynamic> getFromPref(String key) async {
  try {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.getString(key) ?? null;

    return value;
  } catch (e) {
    return null;
  }
}

Future<bool> saveToPref(String key, var value) async {
  try {

    String val = value.toString();

    if (val == null) return false;

    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, val);

    return true;
  } catch (e) {

    print('error $e');

    return false;
  }
}


Future<bool> fetchPrefToken() async {
  try {

    return getFromPref(AppConstants.sharedPrefToken);

  } catch (e) {

    print('error $e');

    return null;
  }
}


Future<bool> clearPref() async{

  try{
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();

    return true;
  }catch(e){

    return false;

  }
}