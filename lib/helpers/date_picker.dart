

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:template_app/app_constants.dart';

String customDatePicker(
    BuildContext context, TextEditingController controller) {
  final format = DateFormat(AppConstants.dateFormatter);

  DateTime dateTime = DateTime.now();

  DatePicker.showDatePicker(context,
      showTitleActions: true,
      minTime: dateTime.subtract(Duration(days: 365000)),
      maxTime: dateTime.add(Duration(days: 365000)), onChanged: (date) {
        controller.text = format.format(date);
      }, onConfirm: (date) {
        controller.text = format.format(date);
      }, currentTime: DateTime.now(), locale: LocaleType.en);

//  print('confirm ${controller.text}');
  return controller.text;
}