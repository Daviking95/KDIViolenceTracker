import 'package:flutter/cupertino.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';

displayImagePickerDialog(
    BuildContext context, Function openCamera, Function openGallery) {
  alertDialogWithTwoButton(
      context: context,
      title: "Select Image/Open Camera",
      alertType: AlertType.info,
      btnText1: "Open Camera",
      function1: openCamera,
      btnText2: "Open Gallery",
      function2: openGallery);
}
