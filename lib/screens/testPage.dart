import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:template_app/helpers/get_location.dart';
import 'package:template_app/model/app_model/app_model.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';

class MyyHomePage extends StatefulWidget {
  @override
  _MyyHomePageState createState() => _MyyHomePageState();
}

class _MyyHomePageState extends State<MyyHomePage> {

  String currentAddress = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Requesting Location Permission'),
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.all(16.0),
                child: Center(
                  child: new Text("Requesting Location Permission...",
                      textAlign: TextAlign.center),
                )),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                  onPressed: () {
                    _getCurrentAddress();
                  },
                  child: Text('Get Location Permission'),
                  color: Colors.green),
            ),
            Center(
              child: Text(
                currentAddress.toString(),
                style: textStylePrimaryColorNormal,
              ),
            )
          ]),
    );
  }

  void _getCurrentAddress() async{
    String addy = await requestLocationPermission(context);
    setState(() {
      currentAddress = addy;

    });
  }
}

class GeolocationExample extends StatefulWidget {
  @override
  _GeolocationExampleState createState() => _GeolocationExampleState();
}

class _GeolocationExampleState extends State<GeolocationExample> {
  Geolocator _geolocator;
  Position _position;

  void checkPermission() {
    _geolocator.checkGeolocationPermissionStatus().then((status) {
      print('status: $status');
    });
    _geolocator
        .checkGeolocationPermissionStatus(
            locationPermission: GeolocationPermission.locationAlways)
        .then((status) {
      print('always status: $status');
    });
    _geolocator.checkGeolocationPermissionStatus(
        locationPermission: GeolocationPermission.locationWhenInUse)
      ..then((status) {
        print('whenInUse status: $status');
      });
  }

  @override
  void initState() {
    super.initState();

    _geolocator = Geolocator();
    LocationOptions locationOptions =
        LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 1);

    checkPermission();
    //    updateLocation();

    StreamSubscription positionStream = _geolocator
        .getPositionStream(locationOptions)
        .listen((Position position) {
      _position = position;
    });
  }

  void updateLocation() async {
    try {
      Position newPosition = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .timeout(new Duration(seconds: 5));

      setState(() {
        _position = newPosition;
      });
    } catch (e) {
      print('Error: ${e.toString()}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
      ),
      body: Center(
          child: Text(
              'Latitude: ${_position != null ? _position.latitude.toString() : '0'},'
              ' Longitude: ${_position != null ? _position.longitude.toString() : '0'}')),
    );
  }
}
