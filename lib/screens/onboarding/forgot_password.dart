import 'package:flutter/material.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/helpers/validators.dart';
import 'package:template_app/model/http_model/forgotPassword.dart';
import 'package:template_app/requests/http_forgot_password.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/starter_widget.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/loader.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final TextEditingController _emailController = new TextEditingController();

  Validators _validators = new Validators();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  @override
  void dispose() {
    // TODO: implement dispose
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
        isBgAllowed: false,
        isFabAllowed: false,
        hasTopWidget: true,
        topPageContent: _topContent(),
        bottomPageContent: _bottomPageContent());
  }

  _topContent() {
    return Container(
      margin: EdgeInsets.only(top: 50.0),
      padding: const EdgeInsets.only(left: 15.0),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.keyboard_backspace,
              size: 30.0,
            ),
          ),
        ],
      ),
    );
  }

  _bottomPageContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Container(
            margin: EdgeInsets.only(top: 50.0),
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        CustomText(
                          title: "Forgot Password",
                          textSize: 25.0,
                          isBold: true,
                          textColor: appPrimaryDarkColor,
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 30.0,
                ),
                AppTextInputs(
                  color: appPrimaryLightColor,
                  controller: _emailController,
                  textInputType: TextInputType.emailAddress,
                  validateInput: _validators.validateEmail,
                  textInputTitle: "Email",
                ),
                SizedBox(
                  height: 80.0,
                ),
                AppPrimaryDarkButtonRound(
                  width: getTargetWidth(context),
                  bgColor: appPrimaryDarkColor,
                  borderRadius: 100.0,
                  context: context,
                  textTitle: "OK",
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: _forgotPassword,
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  _forgotPassword() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        ForgotPassword.email = _emailController.text;

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => AppLoader(
                      context: context,
                      title: "Resetting Password",
                      routeToMoveTo: RoutesConstants.dashboardUrl,
                      successSubTitle: "Password Reset Successful",
                      httpRequestsToRun: resetPasswordRequest,
                    )));
      } catch (e) {
        print(e);
      }
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
