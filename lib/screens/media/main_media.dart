
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/starter_widget.dart';
import 'package:template_app/widgets/temp_app_widget/cards.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class MediaScreen extends StatefulWidget {
  @override
  _MediaScreenState createState() => _MediaScreenState();
}

class _MediaScreenState extends State<MediaScreen> {
  int _currentSelection = 0;
  ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
        isBgAllowed: false,
        isFabAllowed: true,
        hasTopWidget: true,
        hideBackgroundArts: true,
        fabIcon: LineIcons.plus,
        fabFunc: _addReport,
        isBottomBarVisible: true,
        bottomNavIndex: 2,
        topPageContent: _topContent(),
        bottomPageContent: _bottomPageContent());
  }

  _topContent() {
    return Container(
      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
      padding: const EdgeInsets.only(left: 15.0, right: 15.0),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pushReplacementNamed(
                context, RoutesConstants.dashboardUrl),
            child: Icon(
              Icons.keyboard_backspace,
              size: 30.0,
            ),
          ),
//          Spacer(),
//          GestureDetector(
//            onTap: () => Navigator.pop(context),
//            child: Icon(
//              Icons.account_circle,
//              size: 30.0,
//            ),
//          ),
        ],
      ),
    );
  }

  _bottomPageContent() {
    return Container(
      height: MediaQuery.of(context).size.height - 100,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(top: 10),
                children: <Widget>[
                  CupertinoSegmentedControl(
                    borderColor: appPrimaryDarkColor,
                    children: {
                      0: Text("Gallery"),
                      1: Text("Recordings"),
                    },
                    selectedColor: appPrimaryDarkColor,
                    groupValue: _currentSelection,
                    onValueChanged: (value) {
                      setState(() => _currentSelection = value);
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  _currentSelection == 0 ? _tabOne() : _tabTwo(),
                ],
              ),
            ),
          ]),
    );
  }

  _tabOne() {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      padding: EdgeInsets.all(8.0),
      child: ListView.builder(
        primary: false,
        itemCount: 7,
        scrollDirection: Axis.vertical,
        controller: _scrollController,
        reverse: false,
        shrinkWrap: true,
        addAutomaticKeepAlives: true,
        itemBuilder: (context, index) {
          return galleryCardWidget(isFormSubmitted: false);
        },
      ),
    );
  }

  _tabTwo() {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      padding: EdgeInsets.all(8.0),
      child: ListView.builder(
        primary: false,
        itemCount: 7,
        scrollDirection: Axis.vertical,
        controller: _scrollController,
        reverse: false,
        shrinkWrap: true,
        addAutomaticKeepAlives: true,
        itemBuilder: (context, index) {
          return recordingCardWidget(isFormSubmitted: false);
        },
      ),
    );
  }

  _addReport() {
    print('Hello');
//    RouteHelpers.navigateRoute(context: context, routeName: _currentSelection == 0 ? RoutesConstants.addVideoUrl : RoutesConstants.addRecordingUrl, routeType: 8);
  }
}
