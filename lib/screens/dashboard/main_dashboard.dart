import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/starter_widget.dart';
import 'package:template_app/widgets/temp_app_widget/cards.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int _currentSelection = 0;
  ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
        isBgAllowed: false,
        isFabAllowed: true,
        hasTopWidget: true,
        hideBackgroundArts: true,
        fabIcon: LineIcons.plus,
        fabFunc: _addReport,
        isBottomBarVisible: true,
        topPageContent: _topContent(),
        bottomPageContent: _bottomPageContent());
  }

  _topContent() {
    return Container(
      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
      padding: const EdgeInsets.only(left: 15.0, right: 15.0),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          CustomText(
            title: "Home",
            textSize: 25.0,
            isBold: true,
            textColor: appPrimaryDarkColor,
          ),
          Spacer(),
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: GFAvatar(
              backgroundImage:AssetImage(ImageStringConstants.imgKdiLogo),
              shape: GFAvatarShape.circle,
              size: 20,
            ),
          ),
        ],
      ),
    );
  }

  _bottomPageContent() {
    return Container(
      height: MediaQuery.of(context).size.height - 100,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(top: 10),
                children: <Widget>[
                  CupertinoSegmentedControl(
                    borderColor: appPrimaryDarkColor,
                    children: {
                      0: Text("Saved Form"),
                      1: Text("Submitted Form"),
                    },
                    selectedColor: appPrimaryDarkColor,
                    groupValue: _currentSelection,
                    onValueChanged: (value) {
                      setState(() => _currentSelection = value);
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  _currentSelection == 0 ? _tabOne() : _tabTwo(),
                ],
              ),
            ),
          ]),
    );
  }

  _tabOne() {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      padding: EdgeInsets.all(8.0),
      child: ListView.builder(
        primary: false,
        itemCount: 7,
        scrollDirection: Axis.vertical,
        controller: _scrollController,
        reverse: false,
        shrinkWrap: true,
        addAutomaticKeepAlives: true,
        itemBuilder: (context, index) {
          return savedFormCardWidget(isFormSubmitted: false);
        },
      ),
    );
  }

  _tabTwo() {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      padding: EdgeInsets.all(8.0),
      child: ListView.builder(
        primary: false,
        itemCount: 7,
        scrollDirection: Axis.vertical,
        controller: _scrollController,
        reverse: false,
        shrinkWrap: true,
        addAutomaticKeepAlives: true,
        itemBuilder: (context, index) {
          return savedFormCardWidget(isFormSubmitted: true);
        },
      ),
    );
  }

  _addReport() {
    RouteHelpers.navigateRoute(context: context, routeName: RoutesConstants.addReportUrl, routeType: 8);
  }

}

