import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/helpers/validators.dart';
import 'package:template_app/model/http_model/login.dart';
import 'package:template_app/requests/http_login.dart';
import 'package:template_app/screens/onboarding/forgot_password.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/starter_widget.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/loader.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class StarterScreen extends StatefulWidget {
  @override
  _StarterScreenState createState() => _StarterScreenState();
}

class _StarterScreenState extends State<StarterScreen> {
  final TextEditingController _codeController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  Validators _validators = new Validators();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  @override
  void dispose() {
    // TODO: implement dispose
    _codeController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
        isBgAllowed: false,
        isFabAllowed: false,
        centerBottomWidget: true,
        bottomPageContent: _bottomPageContent());
  }

  _bottomPageContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Container(
//            margin: EdgeInsets.only(top: 50.0),
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        CustomText(
                          title: "Welcome to",
                          textSize: 15.0,
                        ),
                        CustomText(
                          title: "Sign In",
                          textSize: 25.0,
                          isBold: true,
                          textColor: appPrimaryDarkColor,
                        )
                      ],
                    ),
                    Image.asset(
                      ImageStringConstants.imgKdiLogo,
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0,
                ),
                AppTextInputs(
                  color: appPrimaryLightColor,
                  controller: _codeController,
                  textInputType: TextInputType.text,
                  validateInput: _validators.validateString,
                  textInputTitle: "Monitor Code",
                  maxLength: 8,
                ),
                AppPasswordTextInput(
                  color: appPrimaryLightColor,
                  controller: _passwordController,
                  validateInput: _validators.validatePassword,
                  textInputTitle: "Password",
                ),
                GestureDetector(
                  onTap: () => RouteHelpers.navigateRoute(context: context, widgetRoute: ForgotPasswordScreen(), routeType: 7 ),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: CustomText(
                      title: "Forgot Password ?",
                      textColor: appSecondaryColor,
                      textSize: 15.0,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.0,
                ),
                AppPrimaryDarkButtonRound(
                  width: getTargetWidth(context),
                  bgColor: appPrimaryDarkColor,
                  borderRadius: 100.0,
                  context: context,
                  textTitle: "Sign In",
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: _loginUser,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      ImageStringConstants.imgVtLogo,
                      width: 100.0,
                    ),
                    Image.asset(
                      ImageStringConstants.imgNevrLogo,
                      width: 100.0,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  _loginUser() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        LoginUser.email = _codeController.text;
        LoginUser.password = _passwordController.text;

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => AppLoader(
                  context: context,
                  title: "Authenticating",
                  routeToMoveTo: RoutesConstants.dashboardUrl,
                  successSubTitle: "Log in Successful",
                  httpRequestsToRun: loginUserRequest,
                )));
      } catch (e) {
        print(e);
      }
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
