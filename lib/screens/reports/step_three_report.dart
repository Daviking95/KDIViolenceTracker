import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/helpers/validators.dart';
import 'package:template_app/screens/reports/add_report.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/custom_spinner.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class StepThreeReport extends StatefulWidget {
  @override
  _StepThreeReportState createState() => _StepThreeReportState();
}

class _StepThreeReportState extends State<StepThreeReport> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  Validators _validators = new Validators();

  final TextEditingController _locationOfIncidentController =
      new TextEditingController();

  final TextEditingController specifylocationOfIncidentController =
      new TextEditingController();

  final TextEditingController _peopleOfIncidentController =
      new TextEditingController();

  final TextEditingController _genderOfIncidentController =
      new TextEditingController();

  final TextEditingController _victimController1 = new TextEditingController();
  final TextEditingController _victimController2 = new TextEditingController();
  final TextEditingController _victimController3 = new TextEditingController();
  final TextEditingController _victimController4 = new TextEditingController();
  final TextEditingController _victimController5 = new TextEditingController();
  final TextEditingController _victimController6 = new TextEditingController();
  final TextEditingController _victimController7 = new TextEditingController();
  final TextEditingController _victimController8 = new TextEditingController();
  final TextEditingController _victimController9 = new TextEditingController();
  final TextEditingController _victimController10 = new TextEditingController();
  final TextEditingController _victimController11 = new TextEditingController();
  final TextEditingController _victimController12 = new TextEditingController();

  bool victimVal1 = false;
  bool victimVal2 = false;
  bool victimVal3 = false;
  bool victimVal4 = false;
  bool victimVal5 = false;
  bool victimVal6 = false;
  bool victimVal7 = false;
  bool victimVal8 = false;
  bool victimVal9 = false;
  bool victimVal10 = false;
  bool victimVal11 = false;
  bool victimVal12 = false;

  String victimString1 =
      "Political party leader or supporter, Candidate or candidate supporter";
  String victimString2 = "Government / State actor";
  String victimString3 = "Party agent";
  String victimString4 = "Election observer / Monitor";
  String victimString5 = "Election worker";
  String victimString6 = "Voter(s)/ Public";
  String victimString7 = "Campaign material";
  String victimString8 =
      "Election office, property, material (like ballots, trucks, warehouse, etc.)";
  String victimString9 = "Political party office or property (specify party)";
  String victimString10 = "State (Gov't) office or property ";
  String victimString11 = "Private property/building";
  String victimString12 = "Other";

  @override
  void initState() {
    // TODO: implement initState
    assignLastValueGotten();
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CustomText(
              title: "Where did the incident happen?",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 10.0,
            ),
            CustomText(
              title:
                  "(Select only one place for the incident, it can be where it started if it moved)",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 20.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                AppSpinner(
                    CustomSpinners.locationOfIncidentSpinners,
                    10.0,
                    "Select where the incident happened ?",
                    _locationOfIncidentController),
//              _locationOfIncidentController.text.isEmpty
//                  ? Container()
//                  :
                Container(
                  margin: EdgeInsets.only(left: 30.0),
                  child: AppTextInputs(
                    color: appPrimaryLightColor,
                    controller: specifylocationOfIncidentController,
                    textInputType: TextInputType.text,
                    textInputTitle: "Specify",
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            CustomText(
              title: "Who did the violence?",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 20.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                AppSpinner(CustomSpinners.peopleOfIncidentSpinners, 10.0,
                    "How many people ?", _peopleOfIncidentController),
                SizedBox(
                  height: 15.0,
                ),
                AppSpinner(CustomSpinners.genderOfIncidentSpinners, 10.0,
                    "What was the gender ?", _genderOfIncidentController),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            CustomText(
              title: "Who were the victims of the violence?",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            CustomText(
              title:
                  "(check all that were vicitms of violence - it is ok if a victim is ALSO a person who did violence)",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 20.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                victimCheckbox(victimString1, victimVal1),
                victimVal1
                    ? _moreMediaOptions(_victimController1,
                        "(specify party or candidate name(s))")
                    : Container(),
                victimCheckbox(victimString2, victimVal2),
                victimVal2
                    ? _moreMediaOptions(_victimController2, "(specify agency)")
                    : Container(),
                victimCheckbox(victimString3, victimVal3),
                victimVal3
                    ? _moreMediaOptions(_victimController3, "(specify party)")
                    : Container(),
                victimCheckbox(victimString4, victimVal4),
                victimVal4
                    ? _moreMediaOptions(_victimController4, "(specify group)")
                    : Container(),
                victimCheckbox(victimString5, victimVal5),
                victimVal5
                    ? _moreMediaOptions(_victimController5)
                    : Container(),
                victimCheckbox(victimString6, victimVal6),
                victimVal6
                    ? _moreMediaOptions(_victimController6)
                    : Container(),
                victimCheckbox(victimString7, victimVal7),
                victimVal7
                    ? _moreMediaOptions(_victimController7)
                    : Container(),
                victimCheckbox(victimString8, victimVal8),
                victimVal8
                    ? _moreMediaOptions(_victimController8)
                    : Container(),
                victimCheckbox(victimString9, victimVal9),
                victimVal9
                    ? _moreMediaOptions(_victimController9, "(specify party)")
                    : Container(),
                victimCheckbox(victimString10, victimVal10),
                victimVal10
                    ? _moreMediaOptions(_victimController10, "(specify name)")
                    : Container(),
                victimCheckbox(victimString11, victimVal11),
                victimVal11
                    ? _moreMediaOptions(_victimController11, "(specify name)")
                    : Container(),
                victimCheckbox(victimString12, victimVal12),
                victimVal12
                    ? _moreMediaOptions(_victimController12)
                    : Container(),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Center(
              child: AppPrimaryDarkButtonRound(
                width: 200,
                bgColor: appPrimaryDarkColor,
                borderRadius: 5.0,
                context: context,
                textTitle: "Next",
                textStyle: textStyleWhiteColorNormal,
                functionToRun: validateStepThreeInputs,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget victimCheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: boolValue,
          activeColor: appPrimaryDarkColor,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Political party leader or supporter, Candidate or candidate supporter":
                  victimVal1 = value;
                  break;
                case "Government / State actor":
                  victimVal2 = value;
                  break;
                case "Party agent":
                  victimVal3 = value;
                  break;
                case "Election observer / Monitor":
                  victimVal4 = value;
                  break;
                case "Election worker":
                  victimVal5 = value;
                  break;
                case "Voter(s)/ Public":
                  victimVal6 = value;
                  break;
                case "Campaign material":
                  victimVal7 = value;
                  break;
                case "Election office, property, material (like ballots, trucks, warehouse, etc.)":
                  victimVal8 = value;
                  break;
                case "Political party office or property (specify party)":
                  victimVal9 = value;
                  break;
                case "State (Gov't) office or property":
                  victimVal10 = value;
                  break;
                case "Private property/building":
                  victimVal11 = value;
                  break;
                case "Other":
                  victimVal12 = value;
                  break;
              }
            });
          },
        ),
        Expanded(
          child: CustomText(
            title: title,
            textSize: 15.0,
            textColor: appBlackColor,
          ),
        ),
      ],
    );
  }

  _moreMediaOptions(TextEditingController victimController,
      [String s = "(Specify)"]) {
    return Container(
      margin: EdgeInsets.only(left: 30.0),
      child: Column(
        children: <Widget>[
          AppTextInputs(
            color: appPrimaryLightColor,
            controller: victimController,
            textInputType: TextInputType.text,
            textInputTitle: "$s",
          ),
        ],
      ),
    );
  }

  validateStepThreeInputs() {
    Map<String, dynamic> locationList = {};

    Map<String, dynamic> causerList = {};

    List<Map<String, dynamic>> victimList = [];

    if (!_validators.spinnerVaidation(_locationOfIncidentController,
        "Please select where the incident happened")) return false;

    if (!_validators.spinnerVaidation(specifylocationOfIncidentController,
        "Please specify how the incident happened")) return false;

    if (!_validators.spinnerVaidation(
        _peopleOfIncidentController, "Please select number of people involved"))
      return false;

    if (!_validators.spinnerVaidation(
        _genderOfIncidentController, "Please select gender")) return false;

    if (victimVal1 &&
        !_validators.spinnerVaidation(
            _victimController1, "Please specify party or candidate name(s)"))
      return false;

    if (victimVal2 &&
        !_validators.spinnerVaidation(
            _victimController2, "Please specify agency")) return false;

    if (victimVal3 &&
        !_validators.spinnerVaidation(
            _victimController3, "Please specify party")) return false;

    if (victimVal4 &&
        !_validators.spinnerVaidation(
            _victimController4, "Please specify group")) return false;

    if (victimVal5 &&
        !_validators.spinnerVaidation(
            _victimController5, "Please specify election worker")) return false;

    if (victimVal6 &&
        !_validators.spinnerVaidation(
            _victimController6, "Please specify Voter(s)/ Public"))
      return false;

    if (victimVal7 &&
        !_validators.spinnerVaidation(
            _victimController7, "Please specify Campaign material"))
      return false;

    if (victimVal8 &&
        !_validators.spinnerVaidation(_victimController8,
            "Please specify Election office, property, material")) return false;

    if (victimVal9 &&
        !_validators.spinnerVaidation(_victimController9,
            "Please specify Political party office or property")) return false;

    if (victimVal10 &&
        !_validators.spinnerVaidation(
            _victimController10, "Please specify name")) return false;

    if (victimVal11 &&
        !_validators.spinnerVaidation(
            _victimController11, "Please specify name")) return false;

    if (victimVal12 &&
        !_validators.spinnerVaidation(_victimController12, "Please specify"))
      return false;

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      locationList = {
        StepThreeReportConstants.locationOfIncident:
            _locationOfIncidentController.text,
        StepThreeReportConstants.specifylocationOfIncident:
            specifylocationOfIncidentController.text
      };

      causerList = {
        StepThreeReportConstants.peopleOfIncident:
            _peopleOfIncidentController.text,
        StepThreeReportConstants.genderOfIncident:
            _genderOfIncidentController.text
      };

      if (victimVal1)
        victimList.add({
          StepThreeReportConstants.victimType: victimString1,
          StepThreeReportConstants.incidentVictimPosition: 1,
          StepThreeReportConstants.victimDesc: _victimController1.text
        });
      if (victimVal2)
        victimList.add({
          StepThreeReportConstants.victimType: victimString2,
          StepThreeReportConstants.incidentVictimPosition: 2,
          StepThreeReportConstants.victimDesc: _victimController2.text
        });
      if (victimVal3)
        victimList.add({
          StepThreeReportConstants.victimType: victimString3,
          StepThreeReportConstants.incidentVictimPosition: 3,
          StepThreeReportConstants.victimDesc: _victimController3.text
        });
      if (victimVal4)
        victimList.add({
          StepThreeReportConstants.victimType: victimString4,
          StepThreeReportConstants.incidentVictimPosition: 4,
          StepThreeReportConstants.victimDesc: _victimController4.text
        });
      if (victimVal5)
        victimList.add({
          StepThreeReportConstants.victimType: victimString5,
          StepThreeReportConstants.incidentVictimPosition: 5,
          StepThreeReportConstants.victimDesc: _victimController5.text
        });
      if (victimVal6)
        victimList.add({
          StepThreeReportConstants.victimType: victimString6,
          StepThreeReportConstants.incidentVictimPosition: 6,
          StepThreeReportConstants.victimDesc: _victimController6.text
        });
      if (victimVal7)
        victimList.add({
          StepThreeReportConstants.victimType: victimString7,
          StepThreeReportConstants.incidentVictimPosition: 7,
          StepThreeReportConstants.victimDesc: _victimController7.text
        });
      if (victimVal8)
        victimList.add({
          StepThreeReportConstants.victimType: victimString8,
          StepThreeReportConstants.incidentVictimPosition: 8,
          StepThreeReportConstants.victimDesc: _victimController8.text
        });
      if (victimVal9)
        victimList.add({
          StepThreeReportConstants.victimType: victimString9,
          StepThreeReportConstants.incidentVictimPosition: 9,
          StepThreeReportConstants.victimDesc: _victimController9.text
        });
      if (victimVal10)
        victimList.add({
          StepThreeReportConstants.victimType: victimString10,
          StepThreeReportConstants.incidentVictimPosition: 10,
          StepThreeReportConstants.victimDesc: _victimController10.text
        });
      if (victimVal11)
        victimList.add({
          StepThreeReportConstants.victimType: victimString11,
          StepThreeReportConstants.incidentVictimPosition: 11,
          StepThreeReportConstants.victimDesc: _victimController11.text
        });
      if (victimVal12)
        victimList.add({
          StepThreeReportConstants.victimType: victimString12,
          StepThreeReportConstants.incidentVictimPosition: 12,
          StepThreeReportConstants.victimDesc: _victimController12.text
        });

      ReportsConstants.addReport[StepThreeReportConstants.incidentLocation] =
          locationList;
      ReportsConstants.addReport[StepThreeReportConstants.peopleInvolved] =
          causerList;
      ReportsConstants.addReport[StepThreeReportConstants.incidentVictim] =
          victimList;

      print(ReportsConstants.addReport);

      alertDialogWithTwoButton(
          context: context,
          title: "Are you sure your data is correct",
          alertType: AlertType.warning,
          btnText1: "No",
          function1: _closeDialog,
          btnText2: "Yes",
          function2: _continueOps);

      return true;
    } else {
      setState(() {
        _autoValidate = true;
      });
      return false;
    }
  }

  _closeDialog() {
    Navigator.pop(context);
  }

  _continueOps() {
    RouteHelpers.navigateRoute(
        context: context,
        widgetRoute: AddReportScreen(
          currentStep: 3,
        ),
        routeType: 7);
  }

  void assignLastValueGotten() {
    if(ReportsConstants.addReport != null && ReportsConstants.addReport[StepThreeReportConstants.incidentLocation] != null){
      _locationOfIncidentController.text = ReportsConstants.addReport[StepThreeReportConstants.incidentLocation][StepThreeReportConstants.locationOfIncident];
      specifylocationOfIncidentController.text = ReportsConstants.addReport[StepThreeReportConstants.incidentLocation][StepThreeReportConstants.specifylocationOfIncident];
    }

    if(ReportsConstants.addReport != null && ReportsConstants.addReport[StepThreeReportConstants.peopleInvolved] != null){
      _peopleOfIncidentController.text = ReportsConstants.addReport[StepThreeReportConstants.peopleInvolved][StepThreeReportConstants.peopleOfIncident];
      _genderOfIncidentController.text = ReportsConstants.addReport[StepThreeReportConstants.peopleInvolved][StepThreeReportConstants.genderOfIncident];
    }

    if (ReportsConstants.addReport != null && ReportsConstants.addReport[StepThreeReportConstants.incidentVictim] != null) {
      for (final val in ReportsConstants
          .addReport[StepThreeReportConstants.incidentVictim]) {
        print(val);
        switch (val[StepThreeReportConstants.incidentVictimPosition]) {
          case 1:
            victimVal1 = true;
            _victimController1.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 2:
            victimVal2 = true;
            _victimController2.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 3:
            victimVal3 = true;
            _victimController3.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 4:
            victimVal4 = true;
            _victimController4.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 5:
            victimVal5 = true;
            _victimController5.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 6:
            victimVal6 = true;
            _victimController6.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 7:
            victimVal7 = true;
            _victimController7.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 8:
            victimVal8 = true;
            _victimController8.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 9:
            victimVal9 = true;
            _victimController9.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 10:
            victimVal10 = true;
            _victimController10.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 11:
            victimVal11 = true;
            _victimController11.text =
            val[StepThreeReportConstants.victimDesc];
            break;
          case 12:
            victimVal12 = true;
            _victimController12.text =
            val[StepThreeReportConstants.victimDesc];
            break;
        }
      }
    }
  }
}
