import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/date_picker.dart';
import 'package:template_app/helpers/get_location.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/helpers/validators.dart';
import 'package:template_app/screens/reports/add_report.dart';
import 'package:template_app/screens/reports/step_two_report.dart';
import 'package:template_app/style.dart';
import 'package:intl/intl.dart';
import 'package:template_app/widgets/custom_spinner.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class StepOneReport extends StatefulWidget {
  @override
  _StepOneReportState createState() => _StepOneReportState();
}

class _StepOneReportState extends State<StepOneReport> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  Validators _validators = new Validators();

  String currentAddress = "";

  final TextEditingController _electionTypeController =
      new TextEditingController();
  final TextEditingController _dateOfIncidenceController =
      new TextEditingController();
  final TextEditingController _timeOfIncidenceController =
      new TextEditingController();
  final TextEditingController _geoPoliticalZoneController =
  new TextEditingController();
  final TextEditingController _cityController =
  new TextEditingController();
  final TextEditingController _stateZoneController =
  new TextEditingController();
  final TextEditingController _incidentTitleController =
  new TextEditingController();
  final TextEditingController _incidentDescriptionController =
  new TextEditingController();

  DateTime date = new DateTime.now();
  final format = DateFormat("dd-MM-yyyy");

  @override
  void dispose() {
    // TODO: implement dispose
    _electionTypeController.dispose();
    _dateOfIncidenceController.dispose();
    _timeOfIncidenceController.dispose();
    _geoPoliticalZoneController.dispose();
    _cityController.dispose();
    _stateZoneController.dispose();
    _incidentTitleController.dispose();
    _incidentDescriptionController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    _getCurrentAddress();
    assignLastValueGotten();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CustomText(
              title: "Monitor's Name : ",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 10.0,
            ),
            CustomText(
              title: "Monitor's Code : ",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 10.0,
            ),
            CustomText(
              title: "Date Of Report : ${format.format(date)}",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 15.0,
            ),
            CustomText(
              title: "Election ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            AppSpinner(CustomSpinners.electionTypeSpinners, 0.0, "Election",
                _electionTypeController),
            SizedBox(
              height: 15.0,
            ),
            CustomText(
              title: "Date of Incidence ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _dateOfIncidenceController,
              textInputType: TextInputType.datetime,
              onTapFunction: _showStartDatePicker,
              validateInput: _validators.validateString,
              textInputTitle: "Select date of incidence",
            ),
            SizedBox(
              height: 15.0,
            ),
            CustomText(
              title: "Time of Incidence ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            AppSpinner(CustomSpinners.timeOfIncidenceSpinners, 0.0,
                "Select time of incidence", _timeOfIncidenceController),
            SizedBox(
              height: 25.0,
            ),
            CustomText(
              title: "Where did the incident happen ? ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _geoPoliticalZoneController,
              textInputType: TextInputType.text,
              validateInput: _validators.validateString,
              textInputTitle: "Geo-Political Zone:",
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _cityController,
              textInputType: TextInputType.text,
              validateInput: _validators.validateString,
              textInputTitle: "Address:",
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _stateZoneController,
              textInputType: TextInputType.text,
              validateInput: _validators.validateString,
              textInputTitle: "State:",
            ),
            SizedBox(
              height: 25.0,
            ),
            CustomText(
              title: "Incident Title : (like a news headline - 3 Party A Supporters Wounded in Clashes with Party B at rally in Abuja) ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _incidentTitleController,
              textInputType: TextInputType.text,
              validateInput: _validators.validateString,
              textInputTitle: "Incident Title:",
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _incidentDescriptionController,
              textInputType: TextInputType.text,
              validateInput: _validators.validateString,
              textInputTitle: "Incident Description:",
              maxLine: 8,
            ),
          Center(
            child: AppPrimaryDarkButtonRound(
              width: 200,
              bgColor: appPrimaryDarkColor,
              borderRadius: 5.0,
              context: context,
              textTitle: "Next",
              textStyle: textStyleWhiteColorNormal,
              functionToRun: validateStepOneInputs,
            ),
          ),
//            SizedBox(
//              height: 20.0,
//            ),
          ],
        ),
      ),
    );
  }

  _showStartDatePicker() {

    _dateOfIncidenceController.text =
        customDatePicker(context, _dateOfIncidenceController);

  }

  void _getCurrentAddress() async{
    String addy = await requestLocationPermission(context);
    setState(() {
      currentAddress = addy;

      _cityController.text = currentAddress;
    });
  }

   validateStepOneInputs(){

    if(!_validators.spinnerVaidation(_electionTypeController, "Please select election type")) return false;

    if(!_validators.spinnerVaidation(_timeOfIncidenceController, "Please select time of incidence")) return false;

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      ReportsConstants.addReport = {
        StepOneReportConstants.electionType : _electionTypeController.text,
        StepOneReportConstants.dateOfIncidence : _dateOfIncidenceController.text,
        StepOneReportConstants.timeOfIncidence : _timeOfIncidenceController.text,
        StepOneReportConstants.geoPoliticalZone : _geoPoliticalZoneController.text,
        StepOneReportConstants.city : _cityController.text,
        StepOneReportConstants.state : _stateZoneController.text,
        StepOneReportConstants.incidentTitle : _incidentTitleController.text,
        StepOneReportConstants.incidentDesc : _incidentDescriptionController.text,

      };

      print(ReportsConstants.addReport);

      alertDialogWithTwoButton(
          context: context,
          title: "Are you sure your data is correct",
          alertType: AlertType.warning,
          btnText1: "No",
          function1: _closeDialog,
          btnText2: "Yes",
          function2: _continueOps);

      return true;

    } else {
      setState(() {
        _autoValidate = true;
      });
      return false;
    }

  }

  _closeDialog() {
    Navigator.pop(context);
  }

  _continueOps() {
    RouteHelpers.navigateRoute(context: context, widgetRoute: AddReportScreen(currentStep: 1,), routeType: 7);
  }

  void assignLastValueGotten() {
    if(ReportsConstants.addReport != null){
      _electionTypeController.text = ReportsConstants.addReport[StepOneReportConstants.electionType];
      _dateOfIncidenceController.text = ReportsConstants.addReport[StepOneReportConstants.dateOfIncidence];
      _timeOfIncidenceController.text = ReportsConstants.addReport[StepOneReportConstants.timeOfIncidence];
      _geoPoliticalZoneController.text = ReportsConstants.addReport[StepOneReportConstants.geoPoliticalZone];
      _cityController.text = ReportsConstants.addReport[StepOneReportConstants.city];
      _stateZoneController.text = ReportsConstants.addReport[StepOneReportConstants.state];
      _incidentTitleController.text = ReportsConstants.addReport[StepOneReportConstants.incidentTitle];
      _incidentDescriptionController.text = ReportsConstants.addReport[StepOneReportConstants.incidentDesc];

    }
  }
}
