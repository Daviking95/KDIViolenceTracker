
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/requests/http_login.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/custom_spinner.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/loader.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class StepFourReport extends StatefulWidget {
  @override
  _StepFourReportState createState() => _StepFourReportState();
}

class _StepFourReportState extends State<StepFourReport> {

  final TextEditingController afterIncidenceController =
  new TextEditingController();

  final TextEditingController specifylocationOfIncidentController =
  new TextEditingController();

  final TextEditingController afterIncidenceDetailsController =
  new TextEditingController();

  final TextEditingController moreReportController =
  new TextEditingController();

  final TextEditingController _weaponController =
  new TextEditingController();

  final TextEditingController _violenceController1 =
  new TextEditingController();
  final TextEditingController _violenceController2 =
  new TextEditingController();
  final TextEditingController _violenceController3 =
  new TextEditingController();
  final TextEditingController _violenceController4 =
  new TextEditingController();
  final TextEditingController _violenceController5 =
  new TextEditingController();
  final TextEditingController _violenceController6 =
  new TextEditingController();
  final TextEditingController _violenceController11 =
  new TextEditingController();
  final TextEditingController _violenceController12 =
  new TextEditingController();

  final TextEditingController _impactController8 =
  new TextEditingController();
  final TextEditingController _impactController9 =
  new TextEditingController();
  final TextEditingController _impactController10 =
  new TextEditingController();
  final TextEditingController _impactController11 =
  new TextEditingController();

  bool violenceVal1 = false;
  bool violenceVal2 = false;
  bool violenceVal3 = false;
  bool violenceVal4 = false;
  bool violenceVal5 = false;
  bool violenceVal6 = false;
  bool violenceVal7 = false;
  bool violenceVal8 = false;
  bool violenceVal9 = false;
  bool violenceVal10 = false;
  bool violenceVal11 = false;
  bool violenceVal12 = false;

  bool weaponVal1 = false;
  bool weaponVal2 = false;
  bool weaponVal3 = false;
  bool weaponVal4 = false;
  bool weaponVal5 = false;
  bool weaponVal6 = false;
  bool weaponVal7 = false;
  bool weaponVal8 = false;
  bool weaponVal9 = false;

  bool impactVal1 = false;
  bool impactVal2 = false;
  bool impactVal3 = false;
  bool impactVal4 = false;
  bool impactVal5 = false;
  bool impactVal6 = false;
  bool impactVal7 = false;
  bool impactVal8 = false;
  bool impactVal9 = false;
  bool impactVal10 = false;
  bool impactVal11 = false;
  bool impactVal12 = false;


  String violenceString1 = "Murder";
  String violenceString2 = "Attempted murder";
  String violenceString3 = "Physical harm / torture";
  String violenceString4 = "Sexual assault";
  String violenceString5 = "Intimidation / psychological abuse";
  String violenceString6 = "Kidnapping";
  String violenceString7 = "Group Clash (groups fight, also check physical harm)";
  String violenceString8 = "Politically motivated arrest or detention";
  String violenceString9 = "Destruction of property";
  String violenceString10 = "Theft";
  String violenceString11 = "Other";

  String weaponString1 = "Fists/physical means";
  String weaponString2 = "Gun/firearm";
  String weaponString3 = "Knives/stabbing";
  String weaponString4 = "Stones/throwing objects";
  String weaponString5 = "Arson";
  String weaponString6 = "Bomb/explosives";
  String weaponString7 = "No weapon";
  String weaponString8 = "Unable to determine";
  String weaponString9 = "Others";

  String impactString1 = "Complaint filed with Electoral Commission";
  String impactString2 = "Civic education event disrupted";
  String impactString3 = "Campaign activity disrupted";
  String impactString4 = "Voting disrupted/voters dispersed or left area";
  String impactString5 = "Disrupted vote count";
  String impactString6 = "Transportation disrupted";
  String impactString7 = "Economic/financial loss";
  String impactString8 = "Cancelled election";
  String impactString9 = "Postponed election";
  String impactString10 = "Re-run election";
  String impactString11 = "Other";
  String impactString12 = "Unable to determine";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CustomText(
            title: "What kind of violence happened?",
            textSize: 15.0,
            isBold: true,
            textColor: appPrimaryDarkColor,
          ),
          SizedBox(
            height: 20.0,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              violenceCheckbox(violenceString1, violenceVal1),
              violenceVal1
                  ? _moreViolenceOptions(_violenceController1, "How many people killed?")
                  : Container(),
              violenceCheckbox(violenceString2, violenceVal2),
              violenceVal2
                  ? _moreViolenceOptions(_violenceController2, "How many people wounded?")
                  : Container(),
              violenceCheckbox(violenceString3, violenceVal3),
              violenceVal3
                  ? _moreViolenceOptions(_violenceController3, "How many people wounded?")
                  : Container(),
              violenceCheckbox(violenceString4, violenceVal4),
              violenceCheckbox(violenceString5, violenceVal5),
              violenceCheckbox(violenceString6, violenceVal6),
              violenceVal6
                  ? _moreViolenceOptions(_violenceController6, "How many people kidnapped?")
                  : Container(),
              violenceCheckbox(violenceString7, violenceVal7),
              violenceCheckbox(violenceString8, violenceVal8),
              violenceCheckbox(violenceString9, violenceVal9),
              violenceCheckbox(violenceString10, violenceVal10),
              violenceCheckbox(violenceString11, violenceVal11),
              violenceVal11
                  ? _moreViolenceOptions(_violenceController11, "(specify)")
                  : Container(),

            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          CustomText(
            title: "What kind of weapon was used?",
            textSize: 15.0,
            isBold: true,
            textColor: appPrimaryDarkColor,
          ),
          SizedBox(
            height: 20.0,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              weaponCheckbox(weaponString1, weaponVal1),
              weaponCheckbox(weaponString2, weaponVal2),
              weaponCheckbox(weaponString3, weaponVal3),
              weaponCheckbox(weaponString4, weaponVal4),
              weaponCheckbox(weaponString5, weaponVal5),
              weaponCheckbox(weaponString6, weaponVal6),
              weaponCheckbox(weaponString7, weaponVal7),
              weaponCheckbox(weaponString8, weaponVal8),
              weaponCheckbox(weaponString9, weaponVal9),
              weaponVal9
                  ? _moreWeaponOptions(_weaponController, "(specify)")
                  : Container(),

            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          CustomText(
            title: "What was the impact of the violence?  That is what were the results?",
            textSize: 15.0,
            isBold: true,
            textColor: appPrimaryDarkColor,
          ),
          SizedBox(
            height: 20.0,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              impactCheckbox(impactString1, impactVal1),
              impactCheckbox(impactString2, impactVal2),
              impactCheckbox(impactString3, impactVal3),
              impactCheckbox(impactString4, impactVal4),
              impactCheckbox(impactString5, impactVal5),
              impactCheckbox(impactString6, impactVal6),
              impactCheckbox(impactString7, impactVal7),
              impactCheckbox(impactString8, impactVal8),
              impactVal8
                  ? _moreImpactOptions(_impactController8)
                  : Container(),
              impactCheckbox(impactString9, impactVal9),
              impactVal9
                  ? _moreImpactOptions(_impactController9)
                  : Container(),
              impactCheckbox(impactString10, impactVal10),
              impactVal10
                  ? _moreImpactOptions(_impactController10)
                  : Container(),
              impactCheckbox(impactString11, impactVal11),
              impactVal11
                  ? _moreImpactOptions(_impactController11)
                  : Container(),
              impactCheckbox(impactString12, impactVal12),

            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          CustomText(
            title: "Did the government, security, parties, or community do anything after this incident?  ",
            textSize: 15.0,
            isBold: true,
            textColor: appPrimaryDarkColor,
          ),
          AppSpinner(CustomSpinners.optionsSpinners, 10.0,
              "Select one option?",
              afterIncidenceController),
          Container(
            child: AppTextInputs(
              color: appPrimaryLightColor,
              controller: afterIncidenceDetailsController,
              textInputType: TextInputType.text,
              textInputTitle: "What did they do? \nFor example, did the police come?",
            ),
          ),
          SizedBox(
            height: 30.0,
          ),
          CustomText(
            title: "Anything else you need to add or have questions about how to report?",
            textSize: 15.0,
            isBold: true,
            textColor: appPrimaryDarkColor,
          ),
          Container(
            child: AppTextInputs(
              color: appPrimaryLightColor,
              controller: moreReportController,
              textInputType: TextInputType.text,
              textInputTitle: "If applicable",
            ),
          ),
          SizedBox(
            height: 30.0,
          ),
          Center(
            child: AppPrimaryDarkButtonRound(
              width: 200,
              bgColor: appPrimaryDarkColor,
              borderRadius: 5.0,
              context: context,
              textTitle: "Next",
              textStyle: textStyleWhiteColorNormal,
              functionToRun: validateStepFourInputs,
            ),
          ),
        ],
      ),
    );
  }

  Widget violenceCheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: boolValue,
          activeColor: appPrimaryDarkColor,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Murder":
                  violenceVal1 = value;
                  break;
                case "Attempted murder":
                  violenceVal2 = value;
                  break;
                case "Physical harm / torture":
                  violenceVal3 = value;
                  break;
                case "Sexual assault":
                  violenceVal4 = value;
                  break;
                case "Intimidation / psychological abuse":
                  violenceVal5 = value;
                  break;
                case "Kidnapping":
                  violenceVal6 = value;
                  break;
                case "Group Clash (groups fight, also check physical harm)":
                  violenceVal7 = value;
                  break;
                case "Politically motivated arrest or detention":
                  violenceVal8 = value;
                  break;
                case "Destruction of property":
                  violenceVal9 = value;
                  break;
                case "Theft":
                  violenceVal10 = value;
                  break;
                case "Other":
                  violenceVal11 = value;
                  break;
              }
            });
          },
        ),
        Expanded(
          child: CustomText(
            title: title,
            textSize: 15.0,
            textColor: appBlackColor,
          ),
        ),
      ],
    );
  }

  _moreViolenceOptions(TextEditingController violenceController, [String s = "(Specify)"]) {
    return Container(
      margin: EdgeInsets.only(left: 30.0),

      child: Column(
        children: <Widget>[
          AppTextInputs(
            color: appPrimaryLightColor,
            controller: violenceController,
            textInputType: TextInputType.text,
            textInputTitle: "$s",
          ),
        ],
      ),
    );
  }


//  ====================================================

  Widget weaponCheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: boolValue,
          activeColor: appPrimaryDarkColor,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Fists/physical means":
                  weaponVal1 = value;
                  break;
                case "Gun/firearm":
                  weaponVal2 = value;
                  break;
                case "Knives/stabbing":
                  weaponVal3 = value;
                  break;
                case "Stones/throwing objects":
                  weaponVal4 = value;
                  break;
                case "Arson":
                  weaponVal5 = value;
                  break;
                case "Bomb/explosives":
                  weaponVal6 = value;
                  break;
                case "No weapon":
                  weaponVal7 = value;
                  break;
                case "Unable to determine":
                  weaponVal8 = value;
                  break;
                case "Others":
                  weaponVal9 = value;
                  break;
              }
            });
          },
        ),
        Expanded(
          child: CustomText(
            title: title,
            textSize: 15.0,
            textColor: appBlackColor,
          ),
        ),
      ],
    );
  }

  _moreWeaponOptions(TextEditingController weaponController, [String s = "(Specify)"]) {
    return Container(
      margin: EdgeInsets.only(left: 30.0),

      child: Column(
        children: <Widget>[
          AppTextInputs(
            color: appPrimaryLightColor,
            controller: weaponController,
            textInputType: TextInputType.text,
            textInputTitle: "$s",
          ),
        ],
      ),
    );
  }


  Widget impactCheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: boolValue,
          activeColor: appPrimaryDarkColor,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Complaint filed with Electoral Commission":
                  impactVal1 = value;
                  break;
                case "Civic education event disrupted":
                  impactVal2 = value;
                  break;
                case "Campaign activity disrupted":
                  impactVal3 = value;
                  break;
                case "Voting disrupted/voters dispersed or left area":
                  impactVal4 = value;
                  break;
                case "Disrupted vote count":
                  impactVal5 = value;
                  break;
                case "Transportation disrupted":
                  impactVal6 = value;
                  break;
                case "Economic/financial loss":
                  impactVal7 = value;
                  break;
                case "Cancelled election":
                  impactVal8 = value;
                  break;
                case "Postponed election":
                  impactVal9 = value;
                  break;
                case "Re-run election":
                  impactVal10 = value;
                  break;
                case "Other":
                  impactVal11 = value;
                  break;
                case "Unable to determine":
                  impactVal12 = value;
                  break;
              }
            });
          },
        ),
        Expanded(
          child: CustomText(
            title: title,
            textSize: 15.0,
            textColor: appBlackColor,
          ),
        ),
      ],
    );
  }

  _moreImpactOptions(TextEditingController sourceController) {
    if (sourceController == _impactController11) {
      return Container(
        margin: EdgeInsets.only(left: 30.0),
        child: AppTextInputs(
          color: appPrimaryLightColor,
          controller: sourceController,
          textInputType: TextInputType.text,
          textInputTitle: "Specify others",
        ),
      );
    }
    if (sourceController == _impactController8 || sourceController == _impactController9 || sourceController == _impactController10) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        margin: EdgeInsets.only(left: 30.0),
        child: Column(
          children: <Widget>[
            AppSpinner(CustomSpinners.impactSpinners, 10.0,
                "Locally or nationally?",
                sourceController),

          ],
        ),
      );
    }
  }

  validateStepFourInputs() {
//    Map<String, dynamic> locationList = {};
//
//    Map<String, dynamic> causerList = {};
//
//    List<Map<String, dynamic>> victimList = [];
//
//    if (!_validators.spinnerVaidation(_locationOfIncidentController,
//        "Please select where the incident happened")) return false;
//
//    if (!_validators.spinnerVaidation(specifylocationOfIncidentController,
//        "Please specify how the incident happened")) return false;
//
//    if (!_validators.spinnerVaidation(
//        _peopleOfIncidentController, "Please select number of people involved"))
//      return false;
//
//    if (!_validators.spinnerVaidation(
//        _genderOfIncidentController, "Please select gender")) return false;
//
//    if (victimVal1 &&
//        !_validators.spinnerVaidation(
//            _victimController1, "Please specify party or candidate name(s)"))
//      return false;
//
//    if (victimVal2 &&
//        !_validators.spinnerVaidation(
//            _victimController2, "Please specify agency")) return false;
//
//    if (victimVal3 &&
//        !_validators.spinnerVaidation(
//            _victimController3, "Please specify party")) return false;
//
//    if (victimVal4 &&
//        !_validators.spinnerVaidation(
//            _victimController4, "Please specify group")) return false;
//
//    if (victimVal5 &&
//        !_validators.spinnerVaidation(
//            _victimController5, "Please specify election worker")) return false;
//
//    if (victimVal6 &&
//        !_validators.spinnerVaidation(
//            _victimController6, "Please specify Voter(s)/ Public"))
//      return false;
//
//    if (victimVal7 &&
//        !_validators.spinnerVaidation(
//            _victimController7, "Please specify Campaign material"))
//      return false;
//
//    if (victimVal8 &&
//        !_validators.spinnerVaidation(_victimController8,
//            "Please specify Election office, property, material")) return false;
//
//    if (victimVal9 &&
//        !_validators.spinnerVaidation(_victimController9,
//            "Please specify Political party office or property")) return false;
//
//    if (victimVal10 &&
//        !_validators.spinnerVaidation(
//            _victimController10, "Please specify name")) return false;
//
//    if (victimVal11 &&
//        !_validators.spinnerVaidation(
//            _victimController11, "Please specify name")) return false;
//
//    if (victimVal12 &&
//        !_validators.spinnerVaidation(_victimController12, "Please specify"))
//      return false;
//
//      locationList = {
//        StepThreeReportConstants.locationOfIncident:
//        _locationOfIncidentController.text,
//        StepThreeReportConstants.specifylocationOfIncident:
//        specifylocationOfIncidentController.text
//      };
//
//      causerList = {
//        StepThreeReportConstants.peopleOfIncident:
//        _peopleOfIncidentController.text,
//        StepThreeReportConstants.genderOfIncident:
//        _genderOfIncidentController.text
//      };
//
//      if (victimVal1)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString1,
//          StepThreeReportConstants.victimDesc: _victimController1.text
//        });
//      if (victimVal2)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString2,
//          StepThreeReportConstants.victimDesc: _victimController2.text
//        });
//      if (victimVal3)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString3,
//          StepThreeReportConstants.victimDesc: _victimController3.text
//        });
//      if (victimVal4)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString4,
//          StepThreeReportConstants.victimDesc: _victimController4.text
//        });
//      if (victimVal5)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString5,
//          StepThreeReportConstants.victimDesc: _victimController5.text
//        });
//      if (victimVal6)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString6,
//          StepThreeReportConstants.victimDesc: _victimController6.text
//        });
//      if (victimVal7)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString7,
//          StepThreeReportConstants.victimDesc: _victimController7.text
//        });
//      if (victimVal8)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString8,
//          StepThreeReportConstants.victimDesc: _victimController8.text
//        });
//      if (victimVal9)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString9,
//          StepThreeReportConstants.victimDesc: _victimController9.text
//        });
//      if (victimVal10)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString10,
//          StepThreeReportConstants.victimDesc: _victimController10.text
//        });
//      if (victimVal11)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString11,
//          StepThreeReportConstants.victimDesc: _victimController11.text
//        });
//      if (victimVal12)
//        victimList.add({
//          StepThreeReportConstants.victimType: victimString12,
//          StepThreeReportConstants.victimDesc: _victimController12.text
//        });
//
//      ReportsConstants.addReport[StepThreeReportConstants.incidentLocation] =
//          locationList;
//      ReportsConstants.addReport[StepThreeReportConstants.peopleInvolved] =
//          causerList;
//      ReportsConstants.addReport[StepThreeReportConstants.incidentVictim] =
//          victimList;
//
//      print(ReportsConstants.addReport);

      alertDialogWithTwoButton(
          context: context,
          title: "Are you sure your data is correct",
          alertType: AlertType.warning,
          btnText1: "No",
          function1: _closeDialog,
          btnText2: "Yes",
          function2: _continueOps);

      return true;

  }

  _closeDialog() {
    Navigator.pop(context);
  }

  _continueOps() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => AppLoader(
              context: context,
              title: "Submitting",
              routeToMoveTo: RoutesConstants.dashboardUrl,
              successSubTitle: "Form submitted",
              httpRequestsToRun: loginUserRequest,
            )));
  }
}
