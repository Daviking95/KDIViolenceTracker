import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/date_picker.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/helpers/validators.dart';
import 'package:template_app/screens/reports/add_report.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/custom_spinner.dart';
import 'package:template_app/widgets/temp_app_widget/alert_dialog.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';
import 'package:template_app/widgets/temp_app_widget/toasts_and_snacks.dart';

class StepTwoReport extends StatefulWidget {
  @override
  _StepTwoReportState createState() => _StepTwoReportState();
}

class _StepTwoReportState extends State<StepTwoReport> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  Validators _validators = new Validators();

  bool sourceVal1 = false;
  bool sourceVal2 = false;
  bool sourceVal3 = false;
  bool sourceVal4 = false;
  bool sourceVal5 = false;
  bool sourceVal6 = false;
  bool sourceVal7 = false;
  bool sourceVal8 = false;
  bool sourceVal9 = false;

  bool mediaVal1 = false;
  bool mediaVal2 = false;
  bool mediaVal3 = false;
  bool mediaVal4 = false;
  bool mediaVal5 = false;

  bool witnessVal1 = false;
  bool witnessVal2 = false;

  bool showMoreWitnessOption = true;

  String sourceString1 = "You (The Monitor) saw the incident";
  String sourceString2 = "Police personel or report";
  String sourceString3 = "Hospital personel or report";
  String sourceString4 = "Election official";
  String sourceString5 = "Election observer";
  String sourceString6 = "Government official";
  String sourceString7 = "NGO representative";
  String sourceString8 = "People in the community";
  String sourceString9 = "Others";

  String mediaString1 = "Newspaper(s)";
  String mediaString2 = "Television program";
  String mediaString3 = "Radio program";
  String mediaString4 = "Others";
  String mediaString5 = "No media";

  String witnessString1 = "Yes";
  String witnessString2 = "No";

  final TextEditingController _sourceController1 = new TextEditingController();
  final TextEditingController _sourceController2 = new TextEditingController();
  final TextEditingController _sourceController3 = new TextEditingController();
  final TextEditingController _sourceController4 = new TextEditingController();
  final TextEditingController _sourceController5 = new TextEditingController();
  final TextEditingController _sourceController6 = new TextEditingController();
  final TextEditingController _sourceController7 = new TextEditingController();
  final TextEditingController _sourceController8 = new TextEditingController();
  final TextEditingController _sourceController9 = new TextEditingController();

  final TextEditingController _mediaController1 = new TextEditingController();
  final TextEditingController _mediaController1B = new TextEditingController();
  final TextEditingController _mediaController2 = new TextEditingController();
  final TextEditingController _mediaController2B = new TextEditingController();
  final TextEditingController _mediaController2C = new TextEditingController();
  final TextEditingController _mediaController3 = new TextEditingController();
  final TextEditingController _mediaController3B = new TextEditingController();
  final TextEditingController _mediaController3C = new TextEditingController();
  final TextEditingController _mediaController4 = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    assignLastValueGotten();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CustomText(
              title: "Who are your sources?",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 10.0,
            ),
            CustomText(
              title: "(Who did you talk to about the incident?) ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // [Monday] checkbox
                sourcesCheckbox(sourceString1, sourceVal1),
                sourceVal1
                    ? _moreWitnessOptions(_sourceController1)
                    : Container(),
                sourcesCheckbox(sourceString2, sourceVal2),
                sourceVal2
                    ? _moreWitnessOptions(_sourceController2)
                    : Container(),
                sourcesCheckbox(sourceString3, sourceVal3),
                sourceVal3
                    ? _moreWitnessOptions(_sourceController3)
                    : Container(),

                sourcesCheckbox(sourceString4, sourceVal4),
                sourceVal4
                    ? _moreWitnessOptions(_sourceController4)
                    : Container(),

                sourcesCheckbox(sourceString5, sourceVal5),
                sourceVal5
                    ? _moreWitnessOptions(_sourceController5)
                    : Container(),

                sourcesCheckbox(sourceString6, sourceVal6),
                sourceVal6
                    ? _moreWitnessOptions(_sourceController6)
                    : Container(),

                sourcesCheckbox(sourceString7, sourceVal7),
                sourceVal7
                    ? _moreWitnessOptions(_sourceController7)
                    : Container(),

                sourcesCheckbox(sourceString8, sourceVal8),
                sourceVal8
                    ? _moreWitnessOptions(_sourceController8)
                    : Container(),

                sourcesCheckbox(sourceString9, sourceVal9),
                sourceVal9
                    ? _moreWitnessOptions(_sourceController9)
                    : Container(),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            CustomText(
              title: "Did you read or hear about it in the media?",
              textSize: 15.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            SizedBox(
              height: 10.0,
            ),
            CustomText(
              title: "(Who did you talk to about the incident?) ",
              textSize: 12.0,
              isBold: true,
              textColor: appPrimaryDarkColor,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // [Monday] checkbox
                mediaCheckbox(mediaString1, mediaVal1),
                mediaVal1
                    ? _moreMediaOptions(
                        _mediaController1, _showDatePicker, _mediaController1B)
                    : Container(),
                mediaCheckbox(mediaString2, mediaVal2),
                mediaVal2
                    ? _moreMediaOptions(_mediaController2, _showDatePicker2,
                        _mediaController2B, _mediaController2C)
                    : Container(),
                mediaCheckbox(mediaString3, mediaVal3),
                mediaVal3
                    ? _moreMediaOptions(_mediaController3, _showDatePicker3,
                        _mediaController3B, _mediaController3C)
                    : Container(),
                mediaCheckbox(mediaString4, mediaVal4),
                mediaVal4 ? _moreMediaOptions(_mediaController4) : Container(),
                mediaCheckbox(mediaString5, mediaVal5),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Center(
              child: AppPrimaryDarkButtonRound(
                width: 200,
                bgColor: appPrimaryDarkColor,
                borderRadius: 5.0,
                context: context,
                textTitle: "Next",
                textStyle: textStyleWhiteColorNormal,
                functionToRun: validateStepTwoInputs,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget sourcesCheckbox(String title, bool boolValue) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Checkbox(
              value: boolValue,
              activeColor: appPrimaryDarkColor,
              onChanged: (bool value) {
                /// manage the state of each value
                setState(() {
                  switch (title) {
                    case "You (The Monitor) saw the incident":
                      sourceVal1 = value;
                      break;
                    case "Police personel or report":
                      sourceVal2 = value;

                      break;
                    case "Hospital personel or report":
                      sourceVal3 = value;

                      break;
                    case "Election official":
                      sourceVal4 = value;

                      break;
                    case "Election observer":
                      sourceVal5 = value;

                      break;
                    case "Government official":
                      sourceVal6 = value;

                      break;
                    case "NGO representative":
                      sourceVal7 = value;

                      break;
                    case "People in the community":
                      sourceVal8 = value;

                      break;
                    case "Others":
                      sourceVal9 = value;

                      break;
                  }
                });
              },
            ),
            Expanded(
              child: CustomText(
                title: title,
                textSize: 15.0,
                textColor: appBlackColor,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget moreOptionsCheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: boolValue,
          activeColor: appPrimaryDarkColor,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Yes":
                  witnessVal1 = value;
                  break;
                case "No":
                  witnessVal2 = value;
                  break;
              }
            });
          },
        ),
        CustomText(
          title: title,
          textSize: 15.0,
          textColor: appBlackColor,
        ),
      ],
    );
  }

  Widget mediaCheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: boolValue,
          activeColor: appPrimaryDarkColor,
          onChanged: (bool value) {
            /// manage the state of each value
            setState(() {
              switch (title) {
                case "Newspaper(s)":
                  mediaVal1 = value;
                  break;
                case "Television program":
                  mediaVal2 = value;
                  break;
                case "Radio program":
                  mediaVal3 = value;
                  break;
                case "Others":
                  mediaVal4 = value;
                  break;
                case "No media":
                  mediaVal5 = value;
                  break;
              }
            });
          },
        ),
        Expanded(
          child: CustomText(
            title: title,
            textSize: 15.0,
            textColor: appBlackColor,
          ),
        ),
      ],
    );
  }

  _moreWitnessOptions(TextEditingController sourceController) {
    if (sourceController == _sourceController9) {
      return Container(
        margin: EdgeInsets.only(left: 30.0),
        child: AppTextInputs(
          color: appPrimaryLightColor,
          controller: sourceController,
          textInputType: TextInputType.text,
          textInputTitle: "Specify others",
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      margin: EdgeInsets.only(left: 30.0),
      child: Column(
        children: <Widget>[
          AppSpinner(CustomSpinners.optionsSpinners, 10.0,
              "Did the SOURCE see the incident happen?", sourceController),
        ],
      ),
    );
  }

  _moreMediaOptions(TextEditingController mediaController,
      [Function datePicker,
      TextEditingController sourceController2,
      TextEditingController sourceController3]) {
    if (mediaController == _mediaController1) {
      return Container(
        margin: EdgeInsets.only(left: 30.0),
        child: Column(
          children: <Widget>[
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: mediaController,
              textInputType: TextInputType.text,
              textInputTitle: "Which newspaper",
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: sourceController2,
              textInputType: TextInputType.datetime,
              onTapFunction: datePicker,
              textInputTitle: "Date",
            ),
          ],
        ),
      );
    } else if (mediaController == _mediaController2 ||
        mediaController == _mediaController3) {
      return Container(
        margin: EdgeInsets.only(left: 30.0),
        child: Column(
          children: <Widget>[
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: mediaController,
              textInputType: TextInputType.text,
              textInputTitle: "Which Channel",
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: sourceController3,
              textInputType: TextInputType.text,
              textInputTitle: "Which Program",
            ),
            AppTextInputs(
              color: appPrimaryLightColor,
              controller: sourceController2,
              textInputType: TextInputType.datetime,
              onTapFunction: datePicker,
              textInputTitle: "What Date",
            ),
          ],
        ),
      );
    } else if (mediaController == _mediaController4)
      return Container(
        margin: EdgeInsets.only(left: 30.0),
        child: AppTextInputs(
          color: appPrimaryLightColor,
          controller: mediaController,
          textInputType: TextInputType.text,
          textInputTitle: "Specify others",
        ),
      );
  }

  _showDatePicker() {
    _mediaController1B.text = customDatePicker(context, _mediaController1B);
  }

  _showDatePicker2() {
    _mediaController2B.text = customDatePicker(context, _mediaController2B);
  }

  _showDatePicker3() {
    _mediaController3B.text = customDatePicker(context, _mediaController3B);
  }

  validateStepTwoInputs() {
    List<Map<String, dynamic>> sourceList = [];

    List<Map<String, dynamic>> mediaList = [];

//    if (!sourceVal1 ||
//        !sourceVal2 ||
//        !sourceVal3 ||
//        !sourceVal4 ||
//        !sourceVal4 ||
//        !sourceVal5 ||
//        !sourceVal6 ||
//        !sourceVal7 ||
//        !sourceVal8 ||
//        !sourceVal9) {
//      showToast(
//          toastMsg: "Select An option from question 1",
//          toastLength: Toast.LENGTH_LONG,
//          toastGravity: ToastGravity.TOP,
//          bgColor: appPrimaryDarkColor,
//          txtColor: appWhiteColor);
//
//      return false;
//    }

    if (sourceVal1 &&
        !_validators.spinnerVaidation(_sourceController1,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal2 &&
        !_validators.spinnerVaidation(_sourceController2,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal3 &&
        !_validators.spinnerVaidation(_sourceController3,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal4 &&
        !_validators.spinnerVaidation(_sourceController4,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal5 &&
        !_validators.spinnerVaidation(_sourceController5,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal6 &&
        !_validators.spinnerVaidation(_sourceController6,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal7 &&
        !_validators.spinnerVaidation(_sourceController7,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal8 &&
        !_validators.spinnerVaidation(_sourceController8,
            "Please select an option from the spinner for checked value under question 1"))
      return false;

    if (sourceVal9 &&
        !_validators.spinnerVaidation(_sourceController9,
            "Please enter value for checked value under question 1"))
      return false;

//    =================================================
    if (mediaVal1 &&
        !_validators.spinnerVaidation(
            _mediaController1, "Please input newspaper name") &&
        !_validators.spinnerVaidation(
            _mediaController1B, "Please input date under newspaper"))
      return false;

    if (mediaVal2 &&
        !_validators.spinnerVaidation(_mediaController2,
            "Please input TV channel under Television program") &&
        !_validators.spinnerVaidation(_mediaController2B,
            "Please input program under Television program") &&
        !_validators.spinnerVaidation(
            _mediaController2C, "Please input date under Television program"))
      return false;

    if (mediaVal3 &&
        !_validators.spinnerVaidation(
            _mediaController3, "Please input TV channel under Radio program") &&
        !_validators.spinnerVaidation(
            _mediaController3B, "Please input program under Radio program") &&
        !_validators.spinnerVaidation(
            _mediaController3C, "Please input date under Radio program"))
      return false;

    if (mediaVal4 &&
        !_validators.spinnerVaidation(
            _mediaController4, "Please specify others")) return false;

//    ===================================================

    if (sourceVal1)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString1,
        StepTwoReportConstants.incidentSourceOptionPosition: 1,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController1.text
      });
    if (sourceVal2)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString2,
        StepTwoReportConstants.incidentSourceOptionPosition: 2,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController2.text
      });
    if (sourceVal3)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString3,
        StepTwoReportConstants.incidentSourceOptionPosition: 3,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController3.text
      });
    if (sourceVal4)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString4,
        StepTwoReportConstants.incidentSourceOptionPosition: 4,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController4.text
      });
    if (sourceVal5)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString5,
        StepTwoReportConstants.incidentSourceOptionPosition: 5,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController5.text
      });
    if (sourceVal6)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString6,
        StepTwoReportConstants.incidentSourceOptionPosition: 6,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController6.text
      });
    if (sourceVal7)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString7,
        StepTwoReportConstants.incidentSourceOptionPosition: 7,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController7.text
      });
    if (sourceVal8)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString8,
        StepTwoReportConstants.incidentSourceOptionPosition: 8,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController8.text
      });
    if (sourceVal9)
      sourceList.add({
        StepTwoReportConstants.incidentSourceOption: sourceString9,
        StepTwoReportConstants.incidentSourceOptionPosition: 9,
        StepTwoReportConstants.didSourceSeeIncident: _sourceController9.text
      });
    //    ===================================================

    if (mediaVal1)
      mediaList.add({
        StepTwoReportConstants.mediaSourceOption: mediaString1,
        StepTwoReportConstants.mediaSourceOptionPosition: 1,
        StepTwoReportConstants.mediaSourceName1: _mediaController1.text,
        StepTwoReportConstants.mediaSourceName2: _mediaController1B.text,
        StepTwoReportConstants.mediaSourceName3: ""
      });
    if (mediaVal2)
      mediaList.add({
        StepTwoReportConstants.mediaSourceOption: mediaString2,
        StepTwoReportConstants.mediaSourceOptionPosition: 2,
        StepTwoReportConstants.mediaSourceName1: _mediaController2.text,
        StepTwoReportConstants.mediaSourceName2: _mediaController2B.text,
        StepTwoReportConstants.mediaSourceName3: _mediaController2C.text,
      });
    if (mediaVal3)
      mediaList.add({
        StepTwoReportConstants.mediaSourceOption: mediaString3,
        StepTwoReportConstants.mediaSourceOptionPosition: 3,
        StepTwoReportConstants.mediaSourceName1: _mediaController3.text,
        StepTwoReportConstants.mediaSourceName2: _mediaController3B.text,
        StepTwoReportConstants.mediaSourceName3: _mediaController3C.text,
      });
    if (mediaVal4)
      mediaList.add({
        StepTwoReportConstants.mediaSourceOption: mediaString4,
        StepTwoReportConstants.mediaSourceOptionPosition: 4,
        StepTwoReportConstants.mediaSourceName1: _mediaController4.text,
        StepTwoReportConstants.mediaSourceName2: "",
        StepTwoReportConstants.mediaSourceName3: "",
      });
    if (mediaVal5)
      mediaList.add({
        StepTwoReportConstants.mediaSourceOption: mediaString5,
        StepTwoReportConstants.mediaSourceOptionPosition: 5,
        StepTwoReportConstants.mediaSourceName1: "",
        StepTwoReportConstants.mediaSourceName2: "",
        StepTwoReportConstants.mediaSourceName3: "",
      });

//    ReportsConstants.addReport.update("a", (value) => value + 100);
    ReportsConstants.addReport[StepTwoReportConstants.incidentSource] =
        sourceList;
    ReportsConstants.addReport[StepTwoReportConstants.mediaSource] = mediaList;

    print(ReportsConstants.addReport);

    alertDialogWithTwoButton(
        context: context,
        title: "Are you sure your data is correct",
        alertType: AlertType.warning,
        btnText1: "No",
        function1: _closeDialog,
        btnText2: "Yes",
        function2: _continueOps);
  }

  _closeDialog() {
    Navigator.pop(context);
  }

  _continueOps() {
    RouteHelpers.navigateRoute(
        context: context,
        widgetRoute: AddReportScreen(
          currentStep: 2,
        ),
        routeType: 7);
  }

  void assignLastValueGotten() {
    if (ReportsConstants.addReport != null && ReportsConstants.addReport[StepTwoReportConstants.incidentSource] != null) {
      for (final val in ReportsConstants
          .addReport[StepTwoReportConstants.incidentSource]) {
        print(val);
        switch (val[StepTwoReportConstants.incidentSourceOptionPosition]) {
          case 1:
            sourceVal1 = true;
            _sourceController1.text =
                val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 2:
            sourceVal2 = true;
            _sourceController2.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 3:
            sourceVal3 = true;
            _sourceController3.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 4:
            sourceVal4 = true;
            _sourceController4.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 5:
            sourceVal5 = true;
            _sourceController5.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 6:
            sourceVal6 = true;
            _sourceController6.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 7:
            sourceVal7 = true;
            _sourceController7.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 8:
            sourceVal8 = true;
            _sourceController8.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
          case 9:
            sourceVal9 = true;
            _sourceController9.text =
            val[StepTwoReportConstants.didSourceSeeIncident];
            break;
        }
      }
    }

    if (ReportsConstants.addReport != null && ReportsConstants.addReport[StepTwoReportConstants.mediaSource] != null) {
      for (final val in ReportsConstants
          .addReport[StepTwoReportConstants.mediaSource]) {
        print(val);
        switch (val[StepTwoReportConstants.mediaSourceOptionPosition]) {
          case 1:
            mediaVal1 = true;
            _mediaController1.text =
            val[StepTwoReportConstants.mediaSourceName1];
            _mediaController1B.text =
            val[StepTwoReportConstants.mediaSourceName2];
            break;
          case 2:
            mediaVal2 = true;
            _mediaController2.text =
            val[StepTwoReportConstants.mediaSourceName1];
            _mediaController2B.text =
            val[StepTwoReportConstants.mediaSourceName2];
            _mediaController2C.text =
            val[StepTwoReportConstants.mediaSourceName3];
            break;
          case 3:
            mediaVal3 = true;
            _mediaController3.text =
            val[StepTwoReportConstants.mediaSourceName1];
            _mediaController3B.text =
            val[StepTwoReportConstants.mediaSourceName2];
            _mediaController3C.text =
            val[StepTwoReportConstants.mediaSourceName3];
            break;
          case 4:
            mediaVal4 = true;
            _mediaController4.text =
            val[StepTwoReportConstants.mediaSourceName1];
            break;
          case 5:
            mediaVal5 = true;
            break;
        }
      }
    }
  }
}
