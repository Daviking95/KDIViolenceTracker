import 'package:flutter/material.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/route_helper.dart';
import 'package:template_app/helpers/validators.dart';
import 'package:template_app/model/http_model/forgotPassword.dart';
import 'package:template_app/requests/http_forgot_password.dart';
import 'package:template_app/requests/http_login.dart';
import 'package:template_app/screens/reports/step_four_report.dart';
import 'package:template_app/screens/reports/step_one_report.dart';
import 'package:template_app/screens/reports/step_three_report.dart';
import 'package:template_app/screens/reports/step_two_report.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/custom_spinner.dart';
import 'package:template_app/widgets/starter_widget.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/loader.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class AddReportScreen extends StatefulWidget {

  int currentStep;

  AddReportScreen({this.currentStep = 0});

  @override
  _AddReportScreenState createState() => _AddReportScreenState();
}

class _AddReportScreenState extends State<AddReportScreen> {
  bool complete = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
        isBgAllowed: false,
        isFabAllowed: false,
        hasTopWidget: true,
        hideBackgroundArts: true,
        isBottomBarVisible: true,
        bottomNavIndex: 1,
        topPageContent: _topContent(),
        bottomPageContent: _bottomPageContent());
  }

  _topContent() {
    return Container(
      margin: EdgeInsets.only(top: 20.0),
      padding: const EdgeInsets.only(left: 15.0),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pushReplacementNamed(
                context, RoutesConstants.dashboardUrl),
            child: Icon(
              Icons.keyboard_backspace,
              size: 30.0,
            ),
          ),
        ],
      ),
    );
  }

  _bottomPageContent() {
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                CustomText(
                  title: "New Incidence Form",
                  textSize: 25.0,
                  isBold: true,
                  textColor: appPrimaryDarkColor,
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              height: MediaQuery.of(context).size.height - 150,
              child: Stepper(
                type: StepperType.horizontal,
                steps: steps,
                currentStep: widget.currentStep,
                onStepContinue: next,
                onStepTapped: (step) => goTo(step),
                onStepCancel: cancel,
                controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) =>
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),
                        AppPrimaryDarkButtonRound(
                          width: 200,
                          bgColor: appSecondaryColor,
                          borderRadius: 5.0,
                          context: context,
                          textTitle: "Cancel",
                          textStyle: textStyleWhiteColorNormal,
                          functionToRun: onStepCancel,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Step> steps = [
    Step(
      title: CustomText(
        title: "1",
        textSize: 10.0,
        isBold: true,
        textColor: appPrimaryDarkColor,
      ),
      isActive: true,
      state: StepState.indexed,
      content: StepOneReport(),
    ),
    Step(
      isActive: true,
      state: StepState.indexed,
      title: CustomText(
        title: "2",
        textSize: 10.0,
        isBold: true,
        textColor: appPrimaryDarkColor,
      ),
      content: StepTwoReport(),
    ),
    Step(
      isActive: true,
      state: StepState.indexed,
      title: CustomText(
        title: "3",
        textSize: 10.0,
        isBold: true,
        textColor: appPrimaryDarkColor,
      ),
      content: StepThreeReport(),
    ),
    Step(
      isActive: true,
      state: StepState.indexed,
      title: CustomText(
        title: "4",
        textSize: 10.0,
        isBold: true,
        textColor: appPrimaryDarkColor,
      ),
      content: StepFourReport(),
    ),
  ];

  next() {
    setState(() {
      widget.currentStep + 1 != steps.length
          ? goTo(widget.currentStep + 1)
          : setState(() => complete = true);
    });

  }

  cancel() {
    setState(() {
      if (widget.currentStep > 0) {
        goTo(widget.currentStep - 1);
      }
    });
  }

  goTo(int step) {
    setState(() => widget.currentStep = step);
  }
}
