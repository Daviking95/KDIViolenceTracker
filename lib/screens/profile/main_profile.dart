

import 'package:flutter/material.dart';
import 'package:getflutter/components/avatar/gf_avatar.dart';
import 'package:getflutter/shape/gf_avatar_shape.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/style.dart';
import 'package:template_app/widgets/starter_widget.dart';
import 'package:template_app/widgets/temp_app_widget/buttons.dart';
import 'package:template_app/widgets/temp_app_widget/inputs.dart';
import 'package:template_app/widgets/temp_app_widget/text.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    _emailController.text = "dave@gmail.com";
    _passwordController.text = "password";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
        isBgAllowed: false,
        isFabAllowed: false,
        hasTopWidget: false,
        hideBackgroundArts: true,
        isBottomBarVisible: true,
        bottomNavIndex: 3,
        bottomPageContent: _bottomPageContent());
  }

  bool isLocationSwitched = true;
  bool isMicrophoneSwitched = true;

  _bottomPageContent() {
    return Container(
      padding: EdgeInsets.all(20.0),
//      margin: EdgeInsets.only(top: 20.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            GFAvatar(
                backgroundImage:AssetImage(ImageStringConstants.imgKdiLogo),
                shape: GFAvatarShape.circle,
              size: 80.0,
            ),
            SizedBox(height: 10.0,),
            CustomText(
              title:
              "Jeffrey Hales",
              textSize: 15.0,
              isBold: true,
              isCenter: true,
              textColor: appBrownGrayOneColor,
            ),
            SizedBox(height: 5.0,),
            CustomText(
              title:
              "Monitor’s Code: HKF56",
              textSize: 12.0,
              isBold: true,
              isCenter: true,
              textColor: appBrownGrayOneColor,
            ),
            SizedBox(height: 20.0,),

            AppTextInputs(
              color: appPrimaryLightColor,
              controller: _emailController,
              textInputType: TextInputType.text,
              textInputTitle: "email",
              isReadOnly: true,
            ),
            AppPasswordTextInput(
              color: appPrimaryLightColor,
              controller: _passwordController,
              textInputTitle: "Password",
            ),
            SizedBox(height: 10.0,),
            Row(
              children: <Widget>[
                CustomText(
                  title:
                  "Location",
                  textSize: 15.0,
                  isBold: true,
                  textColor: appBlackColor,
                ),
                Spacer(),
                Switch(
                  value: isLocationSwitched,
                  onChanged: (value) {
                    setState(() {
                      isLocationSwitched = value;
                      print(isLocationSwitched);
                    });
                  },
                  activeTrackColor: appSecondaryColor.withOpacity(0.5),
                  activeColor: appSecondaryColor,
                )
              ],
            ),
            SizedBox(height: 10.0,),
            Row(
              children: <Widget>[
                CustomText(
                  title:
                  "Microphone",
                  textSize: 15.0,
                  isBold: true,
                  textColor: appBlackColor,
                ),
                Spacer(),
                Switch(
                  value: isMicrophoneSwitched,
                  onChanged: (value) {
                    setState(() {
                      isMicrophoneSwitched = value;
                      print(isMicrophoneSwitched);
                    });
                  },
                  activeTrackColor: appSecondaryColor.withOpacity(0.5),
                  activeColor: appSecondaryColor,
                ),
              ],
            ),

            SizedBox(
              height: 20.0,
            ),
            AppPrimaryDarkButtonRound(
              width: getTargetWidth(context),
              bgColor: appSecondaryColor,
              borderRadius: 100.0,
              context: context,
              textTitle: "Log Out",
              textStyle: textStyleWhiteColorNormal,
              functionToRun: _logoutUser,
            ),
          ]),
    );
  }

  _logoutUser() {
  }
}
