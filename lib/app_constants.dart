
import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AppConstants {
  static bool isIOS =
  defaultTargetPlatform == TargetPlatform.iOS ? true : false;

  static const String appTitle = "KDI Violence Tracker";

  static const String appFontFamily = "AppFontNormal";

  static const String dateFormatter = "dd/MM/yyyy";

  static bool isUserAuthenticated = false;

  static const String sharedPrefToken = "token";

}

class ReportsConstants {

  static Map<String, dynamic> addReport ;
//  Map<String, Object> addReport = new HashMap();
}

class StepOneReportConstants{
  static String electionType = "electionType";
  static String dateOfIncidence = "dateOfIncidence";
  static String timeOfIncidence = "timeOfIncidence";
  static String geoPoliticalZone = "geoPoliticalZone";
  static String city = "city";
  static String state = "state";
  static String incidentTitle = "incidentTitle";
  static String incidentDesc = "incidentDesc";
}

class StepTwoReportConstants{
  static String incidentSourceOption = "incidentSourceOption";
  static String incidentSourceOptionPosition = "incidentSourceOptionPosition";
  static String didSourceSeeIncident = "didSourceSeeIncident";
  static String incidentSource = "incidentSource";

  static String mediaSourceOption = "mediaSourceOption";
  static String mediaSourceOptionPosition = "mediaSourceOptionPosition";
  static String mediaSourceName1 = "nameOne";
  static String mediaSourceName2 = "nameTwo";
  static String mediaSourceName3 = "nameThree";
  static String mediaSource = "mediaSource";

}

class StepThreeReportConstants{
  static String locationOfIncident = "locationOfIncident";
  static String specifylocationOfIncident = "specifylocationOfIncident";
  static String incidentLocation = "incidentLocation";

  static String peopleOfIncident = "peopleOfIncident";
  static String genderOfIncident = "genderOfIncident";
  static String peopleInvolved = "peopleInvolved";

  static String victimType = "victimType";
  static String victimDesc = "victimDesc";
  static String incidentVictim = "incidentVictim";
  static String incidentVictimPosition = "incidentVictimPosition";

}

class ImageStringConstants {
  static const String imgAppLogo = 'assets/images/app-logo.png';

  static const String imgAppFavLogo = 'assets/images/favicon.png';
  static const String imgKdiLogo = 'assets/images/kdi_logo.png';
  static const String imgVtLogo = 'assets/images/vt_logo.png';
  static const String imgNevrLogo = 'assets/images/nevr_logo.png';


  static const String imgAppEllipsePrimary = 'assets/images/ellipse_primary.png';
  static const String imgAppEllipsePrimaryFaded = 'assets/images/ellipse_primary_faded.png';
  static const String imgAppEllipseSecondary = 'assets/images/ellipse_secondary.png';
  static const String imgAppEllipseSecondaryFaded = 'assets/images/ellipse_secondary_faded.png';

}

class RoutesConstants {
  static const String starterUrl = '/';
  static const String dashboardUrl = '/dashboard';
  static const String profileUrl = '/profile';
  static const String mediaUrl = '/media';
  static const String loginUrl = '/login';
  static const String registerUrl = '/register';
  static const String forgotPasswordUrl = '/forgot_password';
  static const String changePasswordUrl = '/change_password';
  static const String addReportUrl = '/add_report';
  static const String otpUrl = '/otp';

}

class HttpDataConstants {
  static var loggedInUserData;
}

class HttpResponseMessage {

  static String _message;

  static String get message => _message;

  static set message(String value) {
    _message = value;
  }

}

class EndpointsConstants{
  static const String baseStUrl = 'https://sterlingproapi.sterlingapps.p.azurewebsites.net/api';
  static const String baseStUrlWithParameter = 'sterlingproapi.sterlingapps.p.azurewebsites.net';
  static const String loginStUrl = '$baseStUrl/Users/Login';
  static const String forgotPasswordUrl = '$baseStUrl/Users/ForgotPassword';

}

double getTargetWidth(BuildContext context) {
  final double deviceWidth = MediaQuery.of(context).size.width;
  final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;
  return targetWidth;
}

double getTargetHeight(BuildContext context) {
  final double deviceHeight = MediaQuery.of(context).size.height;
  final double targetHeight =
  deviceHeight > 550.0 ? 500.0 : deviceHeight * 0.95;
  return targetHeight;
}

class CustomSpinners {

  static List<String> optionsSpinners = [
    'Yes',
    'No'
  ];

  static List<String> impactSpinners = [
    'Locally',
    'Nationally'
  ];

  static List<String> locationOfIncidentSpinners = [
   "In/near polling station",
   "Election office / Facility",
   "Political party office/ Facility",
   "State (Gov't) office/ Property",
   "Private property",
   "Media office",
   "Street / Public area",
   "Others",
  ];


  static List<String> peopleOfIncidentSpinners = [
    "One",
    "More than one but don't know how many",
    "Unable to Determine",
  ];


  static List<String> genderOfIncidentSpinners = [
    "Only Men",
    "Only Women",
    "Both men and women",
    "Unable to Determine",
  ];

  static List<String> electionTypeSpinners = [
    'National Assembly',
    'Presidential',
    'Governor/State Assembly/FCT Area Council',
    'UTD'
  ];

  static List<String> timeOfIncidenceSpinners = [
    'Morning (6am - 12pm)',
    'Afternoon (12pm - 6pm)',
    'Evening (6pm - 12am)',
    'Night (12am - 6am)',
    'Unable to Determine'
  ];
}