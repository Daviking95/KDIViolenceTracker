
class AppModel {

  static double _userCurrentLongitude;

  static double _userCurrentLatitude;

  static String _userCurrentAddress;

  static double get userCurrentLongitude => _userCurrentLongitude;

  static set userCurrentLongitude(double value) {
    _userCurrentLongitude = value;
  }

  static double get userCurrentLatitude => _userCurrentLatitude;

  static String get userCurrentAddress => _userCurrentAddress;

  static set userCurrentAddress(String value) {
    _userCurrentAddress = value;
  }

  static set userCurrentLatitude(double value) {
    _userCurrentLatitude = value;
  }


}