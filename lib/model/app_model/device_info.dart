

class DeviceInfo {

  static String _iMEI;

  static String _deviceType;

  static String _deviceManufacturer;

  static String _deviceModel;

  static String _deviceIP;

  static String _deviceName;

  static String get iMEI => _iMEI;

  static set iMEI(String value) {
    _iMEI = value;
  }

  static String get deviceType => _deviceType;

  static String get deviceName => _deviceName;

  static set deviceName(String value) {
    _deviceName = value;
  }

  static String get deviceIP => _deviceIP;

  static set deviceIP(String value) {
    _deviceIP = value;
  }

  static String get deviceModel => _deviceModel;

  static set deviceModel(String value) {
    _deviceModel = value;
  }

  static String get deviceManufacturer => _deviceManufacturer;

  static set deviceManufacturer(String value) {
    _deviceManufacturer = value;
  }

  static set deviceType(String value) {
    _deviceType = value;
  }


}