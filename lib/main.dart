import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import 'package:template_app/app_constants.dart';
import 'package:template_app/helpers/device_helper.dart';
import 'package:template_app/router.dart';
import 'package:template_app/screens/dashboard/main_dashboard.dart';
import 'package:template_app/screens/start_screen.dart';
import 'package:template_app/screens/testPage.dart';
import 'package:template_app/style.dart';

void main() {
  debugPaintSizeEnabled = false;
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {

    getDeviceInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          title: AppConstants.appTitle,
          theme: appTheme,
          themeMode: ThemeMode.dark,
          initialRoute: RoutesConstants.starterUrl,
          onGenerateRoute: RouteGenerator.generateRoute,
          home: DashboardScreen() // StarterScreen()
      );
  }

}